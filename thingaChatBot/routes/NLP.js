var isDebugNLP = true;
var tag_msg_NLP = "NLP :- ";

function DebugMsgNLP(msg) {
    if (isDebugNLP) {
        console.log(tag_msg_NLP + msg);
    }
}

exports.NLGCommannds = {
    "on_set" : "i have switched on ",
    "off_set": "i have switched off ",
    "brightnessset": "i have set the brightness of  ",
    "colorset": "i have set the Color of  ",
    "getStatus": "the current status of ",
    "getBrightness": "the current Brightness of ",
    "getColor": "the current Color of ",
};

exports.finalStringPreparation = function (objList) {
    return new Promise(function (resolve, reject) {
        var finalStringVal = "";
        for (var index = 0; index < objList.length; index++) {
            var obj = objList[index];
            DebugMsgNLP("obj = " + JSON.stringify(obj));
            if (obj.devicenameOriginal != obj.devicename && index == 0 && obj.roomNameVal != "Default Room") {
                // finalStringVal += obj.devicenameOriginal + " room has " + objList.length + " device/s, ";
                finalStringVal += obj.roomNameVal + " room has " + objList.length + " device/s, ";
            }
            
            if (obj.actionType == "get") {
                DebugMsgNLP("obj  get = ");

                if (obj.deviceattrib.toLowerCase() == "power") {
                    if (index == 0){
                        finalStringVal += exports.NLGCommannds.getStatus;
                        finalStringVal += obj.devicename + ' light is ';
                        finalStringVal += obj.deviceattribVal;
                    } else {
                        finalStringVal += ' and '+ obj.devicename + ' light is ';
                        finalStringVal += obj.deviceattribVal;
                    }
                } else if (obj.deviceattrib.toLowerCase() == "brightness") {
                    if (index == 0) {
                        finalStringVal += exports.NLGCommannds.getBrightness;
                        finalStringVal += obj.devicename + ' light is ';
                        finalStringVal += obj.deviceattribVal + ' percent';
                    } else {
                        finalStringVal += ' and ' + obj.devicename + ' light is ';
                        finalStringVal += obj.deviceattribVal + ' percent';
                    }

                } else if (obj.deviceattrib.toLowerCase() == "color") {
                    if (index == 0) {
                        finalStringVal += exports.NLGCommannds.getColor;
                        finalStringVal += obj.devicename + ' light is ';
                        finalStringVal += obj.deviceattribVal;
                    } else {
                        finalStringVal += ' and ' + obj.devicename + ' light is ';
                        finalStringVal += obj.deviceattribVal;
                    }
                } else if (obj.deviceattrib.toLowerCase() == "switch") {
                    if (index == 0) {
                        finalStringVal += exports.NLGCommannds.getStatus;
                        // finalStringVal += obj.devicename + ' light is ';
                        if(obj.template == "ZwaveBulbs")
                            finalStringVal += obj.devicename + ' light is ';
                        else if(obj.template == "ZwaveSiren")
                            finalStringVal += obj.devicename + ' siren is ';
                        else if(obj.template == "ZwavePlug")
                            finalStringVal += obj.devicename + ' plug is ';
                        finalStringVal += obj.deviceattribVal;
                    } else {
                        // finalStringVal += ' and ' + obj.devicename + ' light is ';
                        if(obj.template == "ZwaveBulbs")
                            finalStringVal += ' and ' + obj.devicename + ' light is ';
                        else if(obj.template == "ZwaveSiren")
                            finalStringVal += ' and ' + obj.devicename + ' siren is ';
                        else if(obj.template == "ZwavePlug")
                            finalStringVal += ' and ' + obj.devicename + ' plug is ';
                        finalStringVal += obj.deviceattribVal;
                    }
                } else {
                    finalStringVal = "your request did  not match, please have a look once";
                }

                // resolve(finalStringVal);
            } else if (obj.actionType == "set") {
                DebugMsgNLP("obj  set = ");
                if (obj.deviceattrib.toLowerCase() == "power") {
                    if (index == 0) {
                        if (obj.deviceattribVal == "on" || obj.deviceattribVal > 0) {
                            finalStringVal += exports.NLGCommannds.on_set;
                        } else {
                            finalStringVal += exports.NLGCommannds.off_set;
                        }
                        finalStringVal += obj.devicename + ' light';
                    } else {
                        finalStringVal += ' and ' + obj.devicename + ' light';
                    }
                } else if (obj.deviceattrib.toLowerCase() == "brightness") {
                    if (index == 0) {
                        finalStringVal += exports.NLGCommannds.brightnessset;
                        finalStringVal += obj.devicename + ' light to ';
                        finalStringVal += obj.deviceattribVal + ' percent';
                    } else {
                        finalStringVal += ' and ' + obj.devicename + ' light to ';
                        finalStringVal += obj.deviceattribVal + ' percent';
                    }

                } else if (obj.deviceattrib.toLowerCase() == "color") {
                    if (index == 0) {
                        finalStringVal += exports.NLGCommannds.colorset;
                        finalStringVal += obj.devicename + ' to ';
                        finalStringVal += obj.deviceattribVal;
                    } else {
                        finalStringVal += ' and ' + obj.devicename + ' to ';
                        finalStringVal += obj.deviceattribVal;
                    }
                } else if (obj.deviceattrib.toLowerCase() == "switch") {
                    if (index == 0) {
                        if (obj.deviceattribVal == "on") {
                            finalStringVal += exports.NLGCommannds.on_set;
                        } else {
                            finalStringVal += exports.NLGCommannds.off_set;
                        }
                        if(obj.template == "ZwaveBulbs")
                            finalStringVal += obj.devicename + ' light';
                        else if(obj.template == "ZwaveSiren")
                            finalStringVal += obj.devicename + ' siren';
                        else if(obj.template == "ZwavePlug")
                            finalStringVal += obj.devicename + ' plug';
                        // finalStringVal += obj.devicename + ' light';
                    } else {
                        if(obj.template == "ZwaveBulbs")
                            finalStringVal += ' and ' + obj.devicename + ' light';
                        else if(obj.template == "ZwaveSiren")
                            finalStringVal += ' and ' + obj.devicename + ' siren';
                        else if(obj.template == "ZwavePlug")
                            finalStringVal += ' and ' + obj.devicename + ' plug';
                        //finalStringVal += ' and ' + obj.devicename + ' light';
                    }
                } else {
                    finalStringVal = "your request did  not match, please have a look once";
                }
                // resolve(finalStringVal);
            } else {
                DebugMsgNLP("obj  unknown ");
                // reject("unknown");
            }
        }
        if (finalStringVal == ""){
            reject("unknown no string matched apparently to tell you");
        } else {
            resolve(finalStringVal);
        }
    });
};