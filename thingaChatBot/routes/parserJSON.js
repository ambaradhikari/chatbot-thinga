var variableFile = require("./variable");
// var NLPFile = require('./NLP');

var isDebugParserJSON = true;
var tag_msg_parserJSON = "parserJSON :- ";
function DebugMsgParserJSON(msg) {
    if (isDebugParserJSON) {
        console.log(tag_msg_parserJSON , msg);
    }
}

exports.devicesNameUsingRoomNameOrDevNameGroup = function(deviceList,roomValArrList) {
  return new Promise(function(resolve, reject) {
    DebugMsgParserJSON("devicesNameUsingRoomNameOrDevNameGroup finalObj");
    var finalObj = roomValArrList;
    DebugMsgParserJSON(finalObj);

    if(finalObj.length > 0) 
    {
        for(var k = 0;k< finalObj.length;k++) 
        {
            var devicenameLowerCase = finalObj[k].value.toLowerCase();
            var deviceNameList = [];
            var deviceNameIDList = [];
            for (var i = 0; i < deviceList.length; i++) {
                var eachDeviceVal = deviceList[i];
                if (eachDeviceVal.template != "ZwaveSensors" && eachDeviceVal.config.roomType && 
                    devicenameLowerCase.indexOf(eachDeviceVal.config.roomType.toLowerCase()) > -1) {
                    deviceNameList.push(eachDeviceVal.name);
                    deviceNameIDList.push(eachDeviceVal.id);
                }
            }

            if (deviceNameList.length == 0) {
                for (var j = 0; j < deviceList.length; j++) {
                    var eachDeviceValChk = deviceList[j];
                    if (eachDeviceValChk.template != "ZwaveSensors" && devicenameLowerCase.indexOf(eachDeviceValChk.name.toLowerCase()) > -1) {
                        // deviceNameList.push(finalObj[k].value);
                        deviceNameList.push(eachDeviceValChk.name);
                        deviceNameIDList.push(eachDeviceValChk.id);
                        break;
                    }

                    if (eachDeviceValChk.template != "ZwaveSensors" && eachDeviceValChk.name.toLowerCase().indexOf(devicenameLowerCase) > -1) {
                        // deviceNameList.push(finalObj[k].value);
                        deviceNameList.push(eachDeviceValChk.name);
                        deviceNameIDList.push(eachDeviceValChk.id);
                        break;
                    }
                }
            }

            if (deviceNameList.length == 0) {
                DebugMsgParserJSON("Not found in Room or device , please try again");
                reject("Not found in Room or device , please try again");
                break;
             } else {
                DebugMsgParserJSON("devicesNameUsingRoomNameOrDevNameGroup deviceNameList = " + JSON.stringify(deviceNameList));
                finalObj[k].value = [];
                finalObj[k].value = deviceNameList;
                finalObj[k].id = [];
                finalObj[k].id = deviceNameIDList;
             }
        }
    }

    DebugMsgParserJSON(finalObj);
    resolve(finalObj);
  });
};

exports.devicesNameUsingRoomNameOrDevName = function(deviceList,parseObj) {
    return new Promise(function (resolve, reject) {
        DebugMsgParserJSON("devicesNameUsingRoomNameOrDevName parseObj = " + JSON.stringify(parseObj));
        var devicenameLowerCase = parseObj.deviceName.toLowerCase();
        var deviceNameList = [];
        var deviceIdList = [];
        for (var i = 0; i < deviceList.length; i++) {
            var eachDeviceVal = deviceList[i];
            if (eachDeviceVal.template != "ZwaveSensors" && eachDeviceVal.config.roomType && 
                devicenameLowerCase.indexOf(eachDeviceVal.config.roomType.toLowerCase()) > -1) {
                deviceNameList.push(eachDeviceVal.name);
                deviceIdList.push(eachDeviceVal.id);
            }
        }

        if (deviceNameList.length == 0) {
            for (var j = 0; j < deviceList.length; j++) {
                var eachDeviceValChk = deviceList[j];
                DebugMsgParserJSON("devicenameLowerCase = "+ devicenameLowerCase);
                DebugMsgParserJSON("eachDeviceValChk.name = "+ eachDeviceValChk.name);
                if (eachDeviceValChk.template != "ZwaveSensors" && devicenameLowerCase.indexOf(eachDeviceValChk.name.toLowerCase()) > -1) {
                    // deviceNameList.push(parseObj.deviceName);
                    deviceNameList.push(eachDeviceValChk.name);
                    deviceIdList.push(eachDeviceValChk.id);
                    break;
                }

                if (eachDeviceValChk.template != "ZwaveSensors" && eachDeviceValChk.name.toLowerCase().indexOf(devicenameLowerCase) > -1) {
                    // deviceNameList.push(parseObj.deviceName);
                    deviceNameList.push(eachDeviceValChk.name);
                    deviceIdList.push(eachDeviceValChk.id);
                    break;
                }
            }
        }

         if (deviceNameList.length == 0) {
            DebugMsgParserJSON("Not found in Room or device , please try again");
            reject("Not found in Room or device , please try again");
         } else {
            DebugMsgParserJSON("devicesNameUsingRoomNameOrDevName deviceNameList = " + JSON.stringify(deviceNameList));
            resolve([deviceNameList,deviceIdList]);
         }
    });
};

exports.getValueFromDBlist = function (HubID, resultVal,DeviceListVal,actionTypeVal) {
    return new Promise(function (resolve, reject) {
        if (resultVal && DeviceListVal) {
            DebugMsgParserJSON("resultVal = " + JSON.stringify(resultVal));
            //DebugMsgParserJSON("DeviceListVal = " + JSON.stringify(DeviceListVal));
            //var result = DeviceListVal;
            var isResolved = false;
            var resultResolveList = [];
            for (var index = 0; index < resultVal.deviceName.length; index++){
                var devicenameLowerCase = resultVal.deviceName[index].toLowerCase();
                var resultResolve = {
                    hubID: "",
                    actionType: "",
                    devicenameOriginal: "",
                    devicename: "",
                    deviceattrib: "",
                    deviceattribVal: ""
                };

                resultResolve.hubID = HubID;
                resultResolve.devicenameOriginal = resultVal.deviceNameOriginal;
                resultResolve.actionType = actionTypeVal;
                // resultResolve.devicename = resultVal.deviceName[index];
                DebugMsgParserJSON('DeviceListVal length = ' + DeviceListVal.length);
                for (var i = 0; i < DeviceListVal.length; i++) {
                    var deviceVal = DeviceListVal[i];
                    DebugMsgParserJSON("///////////////");
                    DebugMsgParserJSON('devicenameLowerCase = ' + devicenameLowerCase);
                    DebugMsgParserJSON('deviceVal.id = ' + deviceVal.id);
                    DebugMsgParserJSON('deviceVal.name = ' + deviceVal.name);
                    DebugMsgParserJSON('deviceVal.template = ' + deviceVal.template);
                    DebugMsgParserJSON('deviceVal.config.roomType = ' + deviceVal.config.roomType);

                    resultResolve.template = deviceVal.template;
                    resultResolve.devicename = deviceVal.name;
                    resultResolve.roomNameVal = deviceVal.config.roomType;

                    // if (devicenameLowerCase.indexOf(deviceVal.name.toLowerCase()) > -1) {
                    if ((devicenameLowerCase.indexOf(deviceVal.name.toLowerCase()) > -1) || 
                        deviceVal.name.toLowerCase().indexOf(devicenameLowerCase) > -1) {
                        for (var j = 0; j < deviceVal.attributes.length; j++) {
                            if (deviceVal.template == "led-light") {
                                if ((resultVal.actionName == "power" || resultVal.actionName == "on" ||
                                    resultVal.actionName == "off") && deviceVal.attributes[j].name == "power") {
                                    resultResolve.deviceattrib = deviceVal.attributes[j].name;
                                    resultResolve.deviceattribVal = deviceVal.attributes[j].value;
                                    // isResolved = true;
                                    // resolve(resultResolve);
                                    resultResolveList.push(resultResolve);
                                } else if (resultVal.actionName == "brightness" && deviceVal.attributes[j].name == "brightness") {
                                    resultResolve.deviceattrib = deviceVal.attributes[j].name;
                                    resultResolve.deviceattribVal = deviceVal.attributes[j].value;
                                    // isResolved = true;
                                    // resolve(resultResolve);
                                    resultResolveList.push(resultResolve);
                                } else if (resultVal.actionName == "color" && deviceVal.attributes[j].name == "color") {
                                    resultResolve.deviceattrib = deviceVal.attributes[j].name;
                                    resultResolve.deviceattribVal = deviceVal.attributes[j].value;
                                    // isResolved = true;
                                    // resolve(resultResolve);
                                    resultResolveList.push(resultResolve);
                                }
                            } else if (deviceVal.template == "ZwaveBulbs") {
                                if((resultVal.actionName == "power" || resultVal.actionName == "on" ||
                                resultVal.actionName == "off") && deviceVal.attributes[j].name.toLowerCase() == "switch") {
                                    resultResolve.deviceattrib = deviceVal.attributes[j].name;
                                    resultResolve.deviceattribVal = deviceVal.attributes[j].value;
                                    // isResolved = true;
                                    // resolve(resultResolve);
                                    resultResolveList.push(resultResolve);
                                } else if (resultVal.actionName == "brightness" && 
                                    deviceVal.attributes[j].name.toLowerCase() == "brightness") {
                                        resultResolve.deviceattrib = deviceVal.attributes[j].name;
                                        resultResolve.deviceattribVal = deviceVal.attributes[j].value;
                                        // isResolved = true;
                                        // resolve(resultResolve);
                                        resultResolveList.push(resultResolve);
                                }

                            } else if (deviceVal.template == "ZwaveSiren" || deviceVal.template == "ZwavePlug"){
                                if ((resultVal.actionName == "power" || resultVal.actionName == "on" ||
                                    resultVal.actionName == "off") && deviceVal.attributes[j].name == "switch") {
                                    resultResolve.deviceattrib = deviceVal.attributes[j].name;
                                    resultResolve.deviceattribVal = deviceVal.attributes[j].value;
                                    // isResolved = true;
                                    // resolve(resultResolve);
                                    resultResolveList.push(resultResolve);
                                }
                            }
                        }
                        break;
                    }
                }
                // if (!isResolved) {
                //     reject("JSON Parser Device or Attribute not found");
                // }
            }

            if (resultResolveList.length == 0){
                reject("JSON Parser Device or Attribute not found");
            } else {
                resolve(resultResolveList);
            }
        } else {
            reject("get value Error");
        }
    });
};

exports.parseBodyHeader = function (body, header) {
    return new Promise(function (resolve, reject) {
        var bodyJSON = body;
        var headerJSON = header;

        var action = "";
        var deviceName = "";
        var actionVal = "Null";

        if (headerJSON && headerJSON.action) {
            action = variableFile.actionStr[headerJSON.action];
            DebugMsgParserJSON('action = '+ action.value);
            if (action.value == "" || action.value == undefined || action.value == null) {
                reject('action.value is null or undefined');
            }
        }

        if (bodyJSON && bodyJSON.slots && bodyJSON.slots.all &&
            bodyJSON.slots.all.value && bodyJSON.slots.all.value == 'true') {
            if (bodyJSON && bodyJSON.slots && bodyJSON.slots.room) {
                deviceName = bodyJSON.slots.room.value;
                if (deviceName == "" || deviceName == undefined ||
                    deviceName == null || bodyJSON.slots.room.remainingLife) {
                    deviceName = "alllights";
                }
                DebugMsgParserJSON('deviceName = ' + deviceName);
            }
        } else if (bodyJSON && bodyJSON.slots && bodyJSON.slots.room) {
            deviceName = bodyJSON.slots.room.value;
            DebugMsgParserJSON('deviceName = ' + deviceName);
            if (deviceName == "" || deviceName == undefined || deviceName == null) {
                // reject('device name is null or undefined');

                DebugMsgParserJSON('if devicename is null or undefined in room slot, then check device slot');
                if (bodyJSON && bodyJSON.slots && bodyJSON.slots.device) {
                    deviceName = bodyJSON.slots.device.value;
                    DebugMsgParserJSON('deviceName = ' + deviceName);
                    if (deviceName == "" || deviceName == undefined || deviceName == null) {
                        reject('device name is null or undefined');
                    }
                } else {
                    reject('device name is null or undefined');
                }

            }
        }

        if (action.value == "brightness"){
            DebugMsgParserJSON('action.value = ' + action.value);
            if (bodyJSON && bodyJSON.slots && bodyJSON.slots.finalvalue && 
                bodyJSON.slots.finalvalue.value && 
                bodyJSON.slots.finalvalue.value != '' &&
                bodyJSON.slots.finalvalue.value != undefined && 
                bodyJSON.slots.finalvalue.value != null) {
                    actionVal = bodyJSON.slots.finalvalue.value;
                    DebugMsgParserJSON('actionVal brightness finalvalue = ' + actionVal);
            } else if (bodyJSON && bodyJSON.slots && bodyJSON.slots.finalvaluepercentage &&
                bodyJSON.slots.finalvaluepercentage.value &&
                bodyJSON.slots.finalvaluepercentage.value != '' &&
                bodyJSON.slots.finalvaluepercentage.value != undefined &&
                bodyJSON.slots.finalvaluepercentage.value != null) {
                    var actionValArray = bodyJSON.slots.finalvaluepercentage.value;
                    actionValArray = actionValArray.split(" ");
                    for (var index = 0; index < actionValArray.length; index++){
                        if (actionValArray[index].toLowerCase() == "percent"){
                            actionVal = actionValArray[index-1];
                        }
                    }
                    //actionVal = actionValArray.split(" ")[0];
                    DebugMsgParserJSON('actionVal brightness finalvaluepercentage = ' + actionVal);
            } else if (bodyJSON && bodyJSON.slots && bodyJSON.slots.changevalue &&
                bodyJSON.slots.changevalue.value &&
                bodyJSON.slots.changevalue.value != '' &&
                bodyJSON.slots.changevalue.value != undefined &&
                bodyJSON.slots.changevalue.value != null) {
                    actionVal = bodyJSON.slots.changevalue.value;
                DebugMsgParserJSON('actionVal brightness changevalue = ' + actionVal);
            } else if (bodyJSON && bodyJSON.slots && bodyJSON.slots.changevaluepercentage &&
                bodyJSON.slots.changevaluepercentage.value &&
                bodyJSON.slots.changevaluepercentage.value != '' &&
                bodyJSON.slots.changevaluepercentage.value != undefined &&
                bodyJSON.slots.changevaluepercentage.value != null) {
                    var actionValArraychg = bodyJSON.slots.changevaluepercentage.value;
                    actionValArraychg = actionValArraychg.split(" ");
                    for (var indexChg = 0; indexChg < actionValArraychg.length; indexChg++) {
                        if (actionValArraychg[indexChg].toLowerCase() == "percent") {
                            actionVal = actionValArraychg[indexChg - 1];
                        }
                    }
                    //actionVal = actionValArraychg.split(" ")[0];
                    DebugMsgParserJSON('actionVal brightness changevaluepercentage = ' + actionVal);
            }
        }

        if (action.value == "color") {
            if (bodyJSON && bodyJSON.slots && bodyJSON.slots.color && bodyJSON.slots.color.value) {
                actionVal = bodyJSON.slots.color.value;
                DebugMsgParserJSON('actionVal color = ' + actionVal);
                if (actionVal == "" || actionVal == undefined || actionVal == null) {
                    reject('Color is null or undefined');
                }
            }
        }

        var obj = {
            actionName: action.value,
            actionType: action.type,
            deviceName: deviceName,
            actionValue: actionVal,
        };

        DebugMsgParserJSON('obj = ' + JSON.stringify(obj));

        if (obj.actionName == '' || obj.deviceName == ''){
            DebugMsgParserJSON("Device name or action name is empty");
            reject("Device name or action name is empty");
        }
        else {
            DebugMsgParserJSON("resolve(obj)");
            resolve(obj);
        }

    });
};