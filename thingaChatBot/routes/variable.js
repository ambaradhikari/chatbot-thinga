exports.actionStr = {
    action_ac_temprature_set_value: {
        type: "set",
    },
    action_brightness_up: {
        type: "set",
    },
    action_color_check: {
        type: "get",
        value: "color"
    },
    action_show_camera_feed: {
        type: "get"
    },
    action_check_brightness: {
        type: "get",
        value: "brightness"
    },
    action_check_brightness_all: {
        type: "get",
    },
    action_check_device_on: {
        type: "get",
        value: "power"
    },
    action_check_lights: {
        type: "get",
        value: "power"
    },
    action_check_lock: {
        type: "get",
    },
    action_check_temprature: {
        type: "get",
    },
    action_check_sensor:{
        type: "get",
    },
    action_heating_down: {
        type: "set",
    },
    action_heating_off:{
        type: "set",
    },
    action_heating_on:{
        type: "set",
    },
    action_heating_schedule_on:{
        type: "set",
    },
    action_heating_up:{
        type: "set",
    },
    action_increase_AC_context: {
        type: "set",
    },
    action_increase_AC_temprature: {
        type: "set",
    },
    action_increase_temprature_value: {
        type: "set"
    },
    action_light_off: {
        type: "set",
        value: "off"
    },
    action_light_on: {
        type: "set",
        value: "on"
    },
    action_lock: {
        type: "set"
    },
    action_mute: {
        type: "set"
    },
    action_powerON_device: {
        type: "set",
        value: "on"
    },
    action_power_off: {
        type: "set",
        value: "off"
    },
    action_power_on: {
        type: "set",
        value: "on"
    },
    action_reduce_brightness: {
        type: "set",
        value: "brightness"
    },
    action_change_color: {
        type: "set",
        value: "color"
    },
    action_schedule_brightness_down: {
        type: "set",
        value: "brightness",
        action: "down"
    },
    action_schedule_brightness_up: {
        type: "set",
        value: "brightness",
        action: "up"
    },
    action_schedule_heating_down: {
        type: "set",
        value: "heating",
        action: "down"
    },
    action_schedule_heating_up: {
        type: "set",
        value: "heating",
        action: "up"
    },
    action_schedule_lights_off: {
        type: "set",
        value: "lights",
        action: "turn off"
    },
    action_schedule_lights_on: {
        type: "set",
        value: "lights",
        action: "turn on"
    },
    action_schedule_off: {
        type: "set",
        value: "power",
        action: "off"
    },
    action_schedule_on: {
        type: "set",
        value: "power",
        action: "on"
    },
    action_schedule_lock: {
        type: "set",
        value: "lock",
        action: "closed"
    },
    action_schedule_unlock: {
        type: "set",
        value: "lock",
        action: "open"
    },
    action_schedule_device_off: {
        type: "set",
        value: "device",
        action: "off"
    },
    action_schedule_device_on: {
        type: "set",
        value: "device",
        action: "on"
    },
    action_motion_routine: {
        type: "set"
    },
    action_set_brightness: {
        type: "set",
        value: "brightness"
    },
    action_set_incremental_brightness: {
        type: "set",
        value: "brightness"
    },
    action_set_volume: {
        type: "set"
    },
    action_switchoff_all: {
        type: "set"
    },
    action_unlock: {
        type: "set"
    },
    action_unmute: {
        type: "set"
    },
    action_volume_check: {
        type: "set"
    },
    action_volume_down: {
        type: "set"
    },
    action_volume_up: {
        type: "set"
    },
    action_zwave_start_pairing: {
        type: "set"
    },
    action_cooking: {
        type: "set"
    },
    zWaveAction: {
        type: "set"
    },
    zWaveCheckPairingStatus: {
        type: "set"
    },
    zWaveSlotAdded: {
        type: "set"
    },
    zwaveStartPairing: {
        type: "set"
    },
    action_volume_context:{

    }
};

exports.actionStringForSwitchLight = [
    "action_color_check", "action_check_brightness", "action_check_lights",
    "action_set_brightness", "action_light_off", "action_light_on",
    "action_powerON_device", "action_power_off", "action_power_on",
    "action_reduce_brightness", "action_change_color","action_set_incremental_brightness",
    "action_check_device_on",
];