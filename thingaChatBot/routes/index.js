var fs = require('fs');
var multer = require('multer');
var s3 = require('s3-node');
var aws = require('aws-sdk');
var bodyParser = require('body-parser');
var shortUrl = require('node-url-shortener');

var express = require('express');
var router = express.Router();
var mqttInit = require("./mqttOperations");
var slackWebhookFile = require('./slackWebhook');
var LightOrSwitchHandler = require('./lightOrSwitchHandleOperations');
var motionSensorHandler = require('./motionSensorRoutineHandler');
var variableFile = require("./variable");
var zwaveAddSlotHandler = require("./zwaveAddSlot");

var checkExpressExtender = require('./expressExtendOperation');

var mongoose = require("mongoose");
var url = "mongodb://thingaDbUser:9caa5b9774ac0cc9899d7b78f283ae97c150da47bfb87077f9f610528fc68c1e@52.52.86.124:27017/thingadb";
mongoose.connect(url);

// for parsing application/json
router.use(bodyParser.json()); 

// for parsing application/xwww-
router.use(bodyParser.urlencoded({ extended: true })); 
//form-urlencoded

var storage = multer.diskStorage({
  destination: function (req, file, cb) {

    console.log("req.body.device_id = "+req.body.device_id);
    console.log("req.body.fileType = "+req.body.fileType);
    // if (fs.existsSync('/home/ubuntu/firmware/'+req.body.device_id)) {
    //   console.log("existsSync = "+req.body.device_id);
    //   if (fs.existsSync('/home/ubuntu/firmware/'+req.body.device_id+'/'+req.body.fileType)) {
    //     console.log("existsSync = "+req.body.device_id +"/" + req.body.fileType);
    //   } else {
    //     fs.mkdirSync('/home/ubuntu/firmware/'+req.body.device_id+'/'+req.body.fileType);
    //   }
    // } else {
    //   console.log("No existsSync create Dir");
    //   fs.mkdirSync('/home/ubuntu/firmware/'+req.body.device_id);
    //   fs.mkdirSync('/home/ubuntu/firmware/'+req.body.device_id+'/'+req.body.fileType);
    // }
    // cb(null, '/home/ubuntu/Chatbot/ExtraData/'+req.body.device_id+'/'+req.body.fileType);
    cb(null, '/home/ubuntu/Chatbot/ExtraData');
  },
  filename: function (req, file, cb) {
    cb(null, file.originalname);
  }
});

var multerUpload = multer({ 
      storage: storage 
                 });

var uploadFile = multerUpload.single('file');

var bucketnameVal = "tantiv4";
// var rootDirInS3 = "rezrv-data";
var rootDirInS3 = "router-data";

aws.config.update({region: 'ap-south-1'});

var awsS3Client = new aws.S3({
    params: {
    Bucket: bucketnameVal,
    region: "ap-south-1"
    },
    signatureVersion: 'v4'
});
var options = {
    s3Client: awsS3Client
};

var client = s3.createClient(options);

/* GET home page. */
router.get('/thingaChatBotGet', function(req, res, next) {
  res.render('index', { title: 'Express Thinga Chatbot : Smart way for the smart IOT Ecosystem' });
});

/* GET home page. */
router.get('/lineChatBotGet', function(req, res, next) {
  res.render('index', { title: 'Express Line Chatbot : Smart way for the smart IOT Ecosystem' });
});

/* GET home page. */
router.get('/webhook', function (req, res, next) {
  res.render('index', { title: 'Express Articulate Chatbot : Smart way for the smart IOT Ecosystem' });
});

router.get('/pddmDataInfoDB', function (req, res, next) {
  console.log("pddmDataInfoDB 321 43");

  const mongoosepddmDB= require('mongoose');

  var dbRoute = "mongodb://tantiv4admin:TANTIV4SOh3TbYhx8ypJPxmt1oOfL@mongo-cluster1-test.tantiv4.com:27017/";

  mongoosepddmDB.connect(dbRoute, { useUnifiedTopology: true , useNewUrlParser: true });

  let db = mongoosepddmDB.connection;

  db.once('open', () => console.log('connected to the database'));

// checks if connection with the database is successful
  db.on('error', console.error.bind(console, 'MongoDB connection error:'));

  // var mongoClientHandler = require('mongodb').MongoClient;
  // var urlPddmDB = "mongodb://tantiv4admin:TANTIV4SOh3TbYhx8ypJPxmt1oOfL@mongo-cluster1-test.tantiv4.com:27017,mongo-cluster2-test.tantiv4.com:27017/";

  // mongoClientHandler.connect(urlPddmDB + 'device?authSource=admin&replicaSet=pddm-rs', { useNewUrlParser: true, useUnifiedTopology: true }, function (err, client) {
  //   console.log('before db -2');
  //   if (err) {
  //     console.log("pddmDataInfoDB error");
  //     var objresSendErr = {
  //       pddmDataInfoDB : 'pddmDataInfoDB error'
  //     };
  //     res.send(objresSendErr);
  //     throw err;
  //   } else {
  //     console.log("pddmDataInfoDB Success mongodb read");
  //     var objresSendSuccess = {
  //       pddmDataInfoDB : 'pddmDataInfoDB success mongodb'
  //     };
  //     res.send(objresSendSuccess);
  //   }
  // });

    var objresSendSuccess = {
      pddmDataInfoDB : 'pddmDataInfoDB success mongodb'
    };
    res.send(objresSendSuccess);

});

router.post('/getFileFromPathAWS', uploadFile, function (req, res) {
  console.log("getFileFromPathAWS Post");
  
  var bucketnameVal = req.body.bucketnameVal; 
  var folderPath = req.body.folderPath;
  var fileName = req.body.fileName;
  var isExpiry = req.body.isExpiry;

  console.log("getFileFromPathAWS bucketnameVal = ",bucketnameVal);
  console.log("getFileFromPathAWS folderPath = ",folderPath);
  console.log("getFileFromPathAWS fileName = ",fileName);
  console.log("getFileFromPathAWS isExpiry = ",isExpiry);

  ////////////////////////////
  aws.config.update({
    region: 'ap-south-1',
  });

  var awsS3Client = new aws.S3({
      params: {
          Bucket: bucketnameVal,
          region: "ap-south-1"
      },
      signatureVersion: 'v4'
  });

  if(isExpiry) {
      var paramsTimedUrl = {Bucket: bucketnameVal, Key: folderPath+fileName, Expires: 60*60};
      var urlTimed = awsS3Client.getSignedUrlPromise('getObject', paramsTimedUrl);
      urlTimed.then(function(url) {
          console.log('isExpiry true, The URL is (expiry in 1 hour)= ' , url);
          function escapeRegExp(string){
              return string.replace(/[.*+?^${}()|[\]\\]/g, "\\$&");
          }
          var urlTimedVal = url.replace(new RegExp(escapeRegExp("&"), 'g'), "%26");
          console.log('isExpiry true , The URL is final= ' + urlTimedVal);

          shortUrl.short(urlTimedVal, function(err, url){
              console.log('isExpiry true (expiry in 1 hour), The URL is shortUrl = ' + url)
              res.send('isExpiry true (expiry in 1 hour), The URL is shortUrl = ' + url);
          });
        }, function(err) { 
          console.log('isExpiry true, Error = ', err);
          res.send('isExpiry true, Error = ', err);
        });
  } else {
      var paramsTimedUrl = {Bucket: bucketnameVal, Key: folderPath+fileName, Expires: 60*60*24*7};
      var urlTimed = awsS3Client.getSignedUrlPromise('getObject', paramsTimedUrl);
      urlTimed.then(function(url) {
          console.log('isExpiry false, The URL is (expiry in 7 days) = ' , url);
          function escapeRegExp(string){
              return string.replace(/[.*+?^${}()|[\]\\]/g, "\\$&");
          }
          var urlTimedVal = url.replace(new RegExp(escapeRegExp("&"), 'g'), "%26");
          console.log('isExpiry false , The URL is final= ' + urlTimedVal);

          shortUrl.short(urlTimedVal, function(err, url){
            console.log('isExpiry false (expiry in 7 days), The URL is shortUrl = ' + url);
            res.send('isExpiry false (expiry in 7 days), The URL is shortUrl = ' + url);
          });
        }, function(err) { 
          console.log('isExpiry false, Error = ', err);
          res.send('isExpiry false, Error = ', err);
        });
  }
  ////////////////////////////
});

router.post('/updateCreateDirAPI', uploadFile, function (req, res) {
  console.log("updateCreateDirAPI postTest");
  console.log("//////////////////////////////////////////////");
  // console.log("obj as device_id= ", req.body.device_id);
  // console.log("obj as fileType= ", req.body.fileType);
  // console.log("obj as fileName= ", req.body.fileName);
  var obj = req.body;
  // console.log("obj as object= ", obj);
  // console.log("obj as string= ", JSON.stringify(obj));

  var deviceID = req.body.device_id;
  var fileType = req.body.fileType; 
  var fileName = req.body.fileName;
  var filePath = '/home/ubuntu/Chatbot/ExtraData/'+req.body.fileName;

  console.log("obj as deviceID= ", deviceID);
  console.log("obj as fileType= ", fileType);
  console.log("obj as fileName= ", fileName);
  console.log("obj as filePath= ", filePath);

  console.log("ApiForCreateDirandUpload");
  var params = {
      s3Params: {
          Bucket: bucketnameVal,
          Delimiter: '/',
      },
  };

  var listObjectsval = client.listObjects(params);
  listObjectsval.on('error', function(err) {
      console.error("unable to ApiForCreateDirandUpload:", err.stack);
      res.send('updateCreateDirAPI postTest Error obj = ' + JSON.stringify(err.stack));
  });

  listObjectsval.on('progress', function() {
      //console.log("progress", listObjectsval.progressAmount);
  });

  listObjectsval.on('end', function() {
      console.log("done ApiForCreateDirandUpload");
  });

  listObjectsval.on('data', function(data) {
    // console.log("lgetCustomerLink istObjectsval data = ",data);
    data.CommonPrefixes.forEach(function(currentValue, index, array){ 
        
        if(currentValue.Prefix.indexOf(rootDirInS3) > -1) {
            console.log("istObjectsval currentValue.Prefix = "+currentValue.Prefix);
            if(currentValue.Prefix === rootDirInS3+'/')
            {
                console.log("getCustomerLink listObjectsval currentValue = "+currentValue.Prefix);

                var date = new Date();
                var n = date.toDateString();
                var time = date.toLocaleTimeString();

                var paramsUpload = {
                    Bucket: bucketnameVal,
                    Body : fs.createReadStream(filePath),
                    // Key : rootDirInS3 + '/' + deviceID + '/' + fileType + '/' + n + ' ' + time + '/' + fileName
                    Key : rootDirInS3 + '/' + deviceID + '/' + fileType + '/' + n + ' ' + time + '_' + fileName
                };

                awsS3Client.upload(paramsUpload, function (err, data) {
                    if (err) {
                        console.log("Error", err);
                        res.send('updateCreateDirAPI postTest Error obj = ' + JSON.stringify(obj));
                    }
        
                    if (data) {
                        console.log("Uploaded in:", data.Location);
                        try {
                            fs.unlinkSync(filePath)
                        } catch(err) {
                            console.error(err)
                        }
                        res.send('updateCreateDirAPI postTest success obj = ' + JSON.stringify(obj));
                    }
                });
              }
          }
      });
  });

  // console.log("//////////////////////////////////////////////");
  // res.send('updateCreateDirAPI postTest success obj = ' + JSON.stringify(obj));
});

router.post('/thingaChatBotPost', function (req, res) {
  console.log("thingaChatBotPost postTest");
  console.log("//////////////////////////////////////////////");
  var obj = req.body;
  console.log("obj as object= ", obj);
  console.log("obj as string= ", JSON.stringify(obj));
  console.log("//////////////////////////////////////////////");
  res.send('thingaChatBotPost postTest success obj = ' + JSON.stringify(obj));
});

router.post('/lineChatBotPost', function (req, res) {
  console.log("lineChatBotPost postTest");
  console.log("//////////////////////////////////////////////");
  var obj = req.body;
  console.log("obj as object= ", obj);
  console.log("obj as string= ", JSON.stringify(obj));
  console.log("//////////////////////////////////////////////");
  res.send('lineChatBotPost postTest success obj = ' +  JSON.stringify(obj));
});

router.post('/webhookLine', function (req, res) {
  console.log("articulateChatBotPost postTest");

  var obj = req.body;
  var objHeaders = req.headers;

  console.log("obj as object= ", obj);
  console.log("obj as objHeaders= ", objHeaders);

  res.send("webhookLine");
});

router.post('/webhook', function (req, res) {
  console.log("articulateChatBotPost postTest");
  console.log("//////////////////////////////////////////////");
  var obj = req.body;
  var objHeaders = req.headers;
  var HubId = "0474162a31a2892";

  console.log("obj as object= ", obj);
  console.log("obj.CSO = ", obj.CSO);
  var CSOFileds = obj.CSO.split(":@:@");
  if(CSOFileds.length > 0){
    HubId = CSOFileds[0];
  } 
  console.log("HubId = ", HubId);
  console.log("obj header as object= ", objHeaders);

  mqttInit.subscribeMqtt("thingoshubUpdDeviceVal/" + HubId);

  if (objHeaders && objHeaders.action) {
    console.log("objHeaders.action index of = ", variableFile.actionStringForSwitchLight.indexOf(objHeaders.action));

    var isLightOrSwitch = variableFile.actionStringForSwitchLight.indexOf(objHeaders.action);
    if (isLightOrSwitch > -1) {
      LightOrSwitchHandler.handlingValues(obj, objHeaders, HubId)
        .then(function (resultLightOrSwitchHandler) {
          console.log("resultLightOrSwitchHandler = ", resultLightOrSwitchHandler);
          res.send("resultLightOrSwitchHandler = " + resultLightOrSwitchHandler);
        })
        .catch(function (errLightOrSwitchHandler) {
          console.log("errLightOrSwitchHandler = ", errLightOrSwitchHandler);
          res.send("errLightOrSwitchHandler = " + errLightOrSwitchHandler);
        });
    } else if (objHeaders.action == "action_motion_routine" || objHeaders.action == "action_schedule_lights_on" ||
                objHeaders.action == "action_schedule_lights_off" || objHeaders.action.indexOf("action_schedule_brightness") > -1) {
       console.log("objHeaders.action = " + objHeaders.action);
       motionSensorHandler.handlingValuesMotionSensor(obj, objHeaders, HubId)
         .then(function (resultmotionSensorHandler) {
           console.log("resultmotionSensorHandler = ", resultmotionSensorHandler);
           res.send("resultmotionSensorHandler = " + resultmotionSensorHandler);
         })
         .catch(function (errmotionSensorHandler) {
           console.log("errmotionSensorHandler = ", errmotionSensorHandler);
           res.send("errmotionSensorHandler = " + errmotionSensorHandler);
         });
    } else if(objHeaders.action == "zWaveSlotAdded") {
      console.log("objHeaders.action = " + objHeaders.action);
      zwaveAddSlotHandler.zWAveAddSlotWithDeviceCheck(HubId, obj, objHeaders)
        .then(function(reszwaveadd) {
          console.log("zWaveSlotAdded resolve true = ",reszwaveadd);
          res.send(reszwaveadd);
        })
        .catch(function(errzwaveadd){
          console.log("zWaveSlotAdded reject false = ",errzwaveadd);
          res.send(errzwaveadd);
        });
    } else if(objHeaders.action == "zWaveStartPairing") {
      console.log("objHeaders.action = " + objHeaders.action);
      zwaveAddSlotHandler.zWAveAddSlotWithDevicePairing(HubId, obj, objHeaders)
        .then(function(reszwaveStartPair) {
          console.log("zWaveStartPairing resolve true = ",reszwaveStartPair);
          res.send(reszwaveStartPair);
        })
        .catch(function(errzwaveStartPair){
          console.log("zWaveStartPairing reject false = ",errzwaveStartPair);
          res.send(errzwaveStartPair);
        });
    } else if(objHeaders.action == "zWaveCheckPairingStatus") {
      console.log("objHeaders.action = " + objHeaders.action);
      zwaveAddSlotHandler.zWAveAddSlotCheckPairingStatus(HubId, obj, objHeaders)
        .then(function(reszwaveStartPair) {
          console.log("zWaveCheckPairingStatus resolve true = ",reszwaveStartPair);
          res.send(reszwaveStartPair);
        })
        .catch(function(errzwaveStartPair){
          console.log("zWaveCheckPairingStatus reject false = ",errzwaveStartPair);
          res.send(errzwaveStartPair);
        });
    } else if(objHeaders.action == "zWaveRemoveDevice") {
      console.log("objHeaders.action = " + objHeaders.action);
      zwaveAddSlotHandler.zWAveRemovedeviceStart(HubId, obj, objHeaders)
        .then(function(reszwaveRemDevStart) {
          console.log("zWaveRemoveDevice resolve true = ",reszwaveRemDevStart);
          res.send(reszwaveRemDevStart);
        })
        .catch(function(errzwaveRemDevStart){
          console.log("zWaveRemoveDevice reject false = ",errzwaveRemDevStart);
          res.send(errzwaveRemDevStart);
        });
    } else if(objHeaders.action == "zWaveCheckRemoval") {
      console.log("objHeaders.action = " + objHeaders.action);
      zwaveAddSlotHandler.zWAveRemoveCheckRemoveStatus(HubId, obj, objHeaders)
        .then(function(reszwaveRemChkStatus) {
          console.log("zWaveCheckRemoval resolve true = ",reszwaveRemChkStatus);
          res.send(reszwaveRemChkStatus);
        })
        .catch(function(errzwaveRemChkStatus){
          console.log("zWaveCheckRemoval reject false = ",errzwaveRemChkStatus);
          res.send(errzwaveRemChkStatus);
        });
    } else if(objHeaders.action == "zWaveReset") {
      console.log("objHeaders.action = " + objHeaders.action);
      zwaveAddSlotHandler.zWAveResetHubStart(HubId, obj, objHeaders)
        .then(function(reszWaveReset) {
          console.log("zWaveReset resolve true = ",reszWaveReset);
          res.send(reszWaveReset);
        })
        .catch(function(errzWaveReset){
          console.log("zWaveReset reject false = ",errzWaveReset);
          res.send(errzWaveReset);
        });
    } else if(objHeaders.action == "zWaveCheckReset") {
      console.log("objHeaders.action = " + objHeaders.action);
      zwaveAddSlotHandler.zWAveResetHubCheckStatus(HubId, obj, objHeaders)
        .then(function(reszWaveCheckReset) {
          console.log("zWaveCheckReset resolve true = ",reszWaveCheckReset);
          res.send(reszWaveCheckReset);
        })
        .catch(function(errzWaveCheckReset){
          console.log("zWaveCheckReset reject false = ",errzWaveCheckReset);
          res.send(errzWaveCheckReset);
        });
    } else {
      console.log("command/action not handled yet");
      res.send("command/action not handled yet");
    }

  }

  var promiseSlackWebhook = slackWebhookFile.slackWebhookcall(obj, objHeaders);
  promiseSlackWebhook.then(function (result) {
    console.log("promiseSlackWebhook response = " + result);
  }, function (err) {
    console.log("promiseSlackWebhook err = " + err);
  });

  console.log("//////////////////////////////////////////////");
  // res.send('articulateChatBotPost postTest success obj = ' + JSON.stringify(obj));
});

module.exports = router;
