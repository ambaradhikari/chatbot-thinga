var request = require('request'); 

var isDebugzwaveAddHdlr = true;
var tag_msg_isDebugzwaveAddHdlr = "zwaveAddHandler :- ";

function DebugzwaveAddHdlr(msg) {
    if (isDebugzwaveAddHdlr) {
        console.log(tag_msg_isDebugzwaveAddHdlr, msg);
    }
}

function mqttFunctionForZwaveStatusCheck(topic, msg, HubId) {
    return new Promise(function (resolveMqtt, rejectMqtt) {

        ///////////////

        var mqttChatBot = require('mqtt');
        // var mMQTTURL = "mqtt-dev.tantiv4.com";
        var mMQTTURL = "ritehub-mqtt.tantiv4.com";
        var clientMqttDevice;

        var optionsforMqttDevice = {
            // port: 8883,
            port: 1883,
            host: mMQTTURL,
            // clientId: 'mqttjs_'+topic,
            clientId: 'mqttjsBTstar_chatBot_Thinga'+topic,
            keepalive: 60,
            reconnectPeriod: 1000,
            protocolId: 'MQIsdp',
            protocolVersion: 3,
            clean: true,
            encoding: 'utf8',
            rejectUnauthorized: false,
            // username: "thingoshub",
            // password: "thingoshub"
            username: "thingaoshub",
            password: "thingaoshub"
        };

        DebugzwaveAddHdlr("#############################");
        DebugzwaveAddHdlr("mqttFunctionForZwaveStatusCheck");
        DebugzwaveAddHdlr("clientMqttDevice mMQTTURL = " + mMQTTURL);
        //clientMqttDevice = mqttChatBot.connect('mqtts://' + mMQTTURL, optionsforMqttDevice);
        clientMqttDevice = mqttChatBot.connect('mqtt://' + mMQTTURL, optionsforMqttDevice);
        var toCloseMqttConn = false;
        var timeouttoCloseMqttConn;
        clientMqttDevice.on('connect', function(err) {
            DebugzwaveAddHdlr("clientMqttDevice PDDM connect toCloseMqttConn = "+toCloseMqttConn);
            if(toCloseMqttConn == true)
            {
                //To Do
            }
            else
            {
                toCloseMqttConn = true;
                DebugzwaveAddHdlr("clientMqttDevice PDDM connect topic check = "+topic);
                clientMqttDevice.subscribe(topic + '/get/state');

                timeouttoCloseMqttConn = setTimeout(function() {
                    DebugzwaveAddHdlr("clientMqttDeviceBTStar timeouttoCloseMqttConn connected toCloseMqttConn = " + toCloseMqttConn);
                    if(toCloseMqttConn == true) {
                        toCloseMqttConn = false;
                        clientMqttDevice.unsubscribe(topic + '/get/state');
                        clientMqttDevice.end();
                        DebugzwaveAddHdlr("clientMqttDeviceBTStar.end()");
                        var objFail = { status: 'false' };
                        rejectMqtt(objFail);
                    } 
                }, 60000);
            }
        });

        clientMqttDevice.on('message', function (topic, message) {
            DebugzwaveAddHdlr("clientMqttDevice message topic = " + topic);
            DebugzwaveAddHdlr("clientMqttDevice message message = " + message);

            if(topic.indexOf("/get/state") > -1)
            {
                toCloseMqttConn = false;
                clientMqttDevice.unsubscribe(topic);
                var objSuc = { status: 'completed' };
                var objFail = { status: 'false' };
                var messageJsonParsed = JSON.parse(message);
                DebugzwaveAddHdlr("messageJsonParsed = ");
                DebugzwaveAddHdlr(messageJsonParsed);
                DebugzwaveAddHdlr("messageJsonParsed.msg = " + messageJsonParsed.msg);
                messageJsonParsed.zwaveTemplateList.ZwaveTemplates.push(messageJsonParsed.newDeviceObj);
                DebugzwaveAddHdlr("messageJsonParsed after update = ");
                DebugzwaveAddHdlr(messageJsonParsed);
                DebugzwaveAddHdlr("messageJsonParsed.zwaveTemplateList after update = ");
                DebugzwaveAddHdlr(messageJsonParsed.zwaveTemplateList);
                if(topic.indexOf("ZwaveCheckPairingStatusChatbot") > -1){
                    //if(message.indexOf("Pairing-Completed")> -1){
                    if(messageJsonParsed.msg.indexOf("Pairing-Completed")> -1){
                        clientMqttDevice.publish("ZwavechatBotsendUpdZwaveTemplate/" + HubId,JSON.stringify(messageJsonParsed.zwaveTemplateList));
                        resolveMqtt(objSuc);
                    } else {
                        rejectMqtt(objFail);
                    }
                }

                clearInterval(timeouttoCloseMqttConn);
                clientMqttDevice.end();
            }
        });

        ///////////////

    });
}

function mqttFunctionForZwaveAddRemoveSlot(topic, msg, HubId) {
    return new Promise(function (resolveMqtt, rejectMqtt) {

        ///////////////

        var mqttChatBot = require('mqtt');
        // var mMQTTURL = "mqtt-dev.tantiv4.com";
        var mMQTTURL = "ritehub-mqtt.tantiv4.com";
        var clientMqttDevice;

        var optionsforMqttDevice = {
            // port: 8883,
            port: 1883,
            host: mMQTTURL,
            // clientId: 'mqttjs_'+topic,
            clientId: 'mqttjsBTstar_chatBot_Thinga'+topic,
            keepalive: 60,
            reconnectPeriod: 1000,
            protocolId: 'MQIsdp',
            protocolVersion: 3,
            clean: true,
            encoding: 'utf8',
            rejectUnauthorized: false,
            // username: "thingoshub",
            // password: "thingoshub"
            username: "thingaoshub",
            password: "thingaoshub"
        };

        DebugzwaveAddHdlr("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
        DebugzwaveAddHdlr("mqttFunctionForZwaveAddRemoveSlot");

        DebugzwaveAddHdlr("clientMqttDevice mMQTTURL = " + mMQTTURL);
        // clientMqttDevice = mqttChatBot.connect('mqtts://' + mMQTTURL, optionsforMqttDevice);
        clientMqttDevice = mqttChatBot.connect('mqtt://' + mMQTTURL, optionsforMqttDevice);
        var toCloseMqttConn = false;
        var timeouttoCloseMqttConn;
        clientMqttDevice.on('connect', function(err) {
            DebugzwaveAddHdlr("clientMqttDevice PDDM connect toCloseMqttConn = "+toCloseMqttConn);
            if(toCloseMqttConn == true)
            {
                toCloseMqttConn = false;
                clientMqttDevice.unsubscribe(topic + '/get/state');
                var objFail = { status: 'false' };
                rejectMqtt(objFail);
                clientMqttDevice.end();
            }
            else
            {
                toCloseMqttConn = true;
                DebugzwaveAddHdlr("clientMqttDevice PDDM connect topic check = "+topic);
                clientMqttDevice.subscribe(topic + '/get/state');
                clientMqttDevice.publish(topic,JSON.stringify(msg));

                timeouttoCloseMqttConn = setTimeout(function() {
                    DebugzwaveAddHdlr("clientMqttDeviceBTStar timeouttoCloseMqttConn connected toCloseMqttConn = " + toCloseMqttConn);
                    if(toCloseMqttConn == true) {
                        toCloseMqttConn = false;
                        clientMqttDevice.unsubscribe(topic + '/get/state');
                        clientMqttDevice.end();
                        DebugzwaveAddHdlr("clientMqttDeviceBTStar.end()");
                        var objFail = { status: 'false' };
                        rejectMqtt(objFail);
                    } 
                }, 20000);
            }
        });

        clientMqttDevice.on('message', function (topic, message) {
            DebugzwaveAddHdlr("clientMqttDevice message topic = " + topic);
            DebugzwaveAddHdlr("clientMqttDevice message message = " + message);

            if(topic.indexOf("/get/state") > -1)
            {
                toCloseMqttConn = false;
                clientMqttDevice.unsubscribe(topic);
                var objSuc = { status: 'true' };
                var objFail = { status: 'false' };

                if(topic.indexOf("addremoveZwaveDeviceChatbot") > -1){
                    if(message == "true"){
                        resolveMqtt(objSuc);
                    } else {
                        rejectMqtt(objFail);
                    }
                } 

                clearInterval(timeouttoCloseMqttConn);
                clientMqttDevice.end();
            }
        });

        ///////////////

    });
}

exports.zWAveAddSlotWithDevicePairing = function (HubId, obj, objHeaders) {
    return new Promise(function (resolve, reject) {
        if(obj.slots && obj.slots.zWaveDeviceType && obj.slots.zWaveDeviceType.value) {
            DebugzwaveAddHdlr("obj.slots.zWaveDeviceType.value = " + obj.slots.zWaveDeviceType.value);
            mqttFunctionForZwaveAddRemoveSlot("addremoveZwaveDeviceChatbot/" + HubId, obj.slots.zWaveDeviceType.value, HubId)
              .then(function (resultMqttExt) {
                DebugzwaveAddHdlr("zWAveAddSlotWithDevicePairing resultMqttExt :-");
                DebugzwaveAddHdlr(resultMqttExt);
                resolve(resultMqttExt);
              })
              .catch(function(errMqttExt){
                DebugzwaveAddHdlr("zWAveAddSlotWithDevicePairing errMqttExt :-");
                DebugzwaveAddHdlr(errMqttExt);
                reject(errMqttExt);
              });
          } else {
            DebugzwaveAddHdlr("Type of zwave device Not Specified");
            reject("Type of zwave device Not Specified");
          }
    });
};

exports.zWAveAddSlotWithDeviceCheck = function(HubId, obj, objHeaders){
    return new Promise(function (resolve, reject) {
        if(obj.slots && obj.slots.zWaveDeviceType && obj.slots.zWaveDeviceType.value) {
            var listZwaveDevice = ["Bulb","Lock","Siren","Plug","Sensors","Others"];
            if(listZwaveDevice.indexOf(obj.slots.zWaveDeviceType.value) > -1) {
                var objSuc = { status: 'true' };
                resolve(objSuc);
            } else {
                var objFail = { status: 'false' };
                reject(objFail);
            }
                
        } else {
            DebugzwaveAddHdlr("Type of zwave device Not Specified");
            reject("Type of zwave device Not Specified");
        }
    });
};

exports.zWAveAddSlotCheckPairingStatus = function (HubId, obj, objHeaders) {
    return new Promise(function (resolve, reject) {
        mqttFunctionForZwaveStatusCheck("ZwaveCheckPairingStatusChatbot/" + HubId, "zWaveCheckPairingStatus", HubId)
            .then(function (resultMqttExt) {
            DebugzwaveAddHdlr("zWAveAddSlotCheckPairingStatus resultMqttExt :-");
            DebugzwaveAddHdlr(resultMqttExt);
            resolve(resultMqttExt);
            })
            .catch(function(errMqttExt){
            DebugzwaveAddHdlr("zWAveAddSlotCheckPairingStatus errMqttExt :-");
            DebugzwaveAddHdlr(errMqttExt);
            reject(errMqttExt);
            });
    });
};

function mqttFunctionForZwaveRemDevStatusCheck(topic,HubId) {
    return new Promise(function (resolveMqtt, rejectMqtt) {
        ///////////////

        var mqttChatBot = require('mqtt');
        // var mMQTTURL = "mqtt-dev.tantiv4.com";
        var mMQTTURL = "ritehub-mqtt.tantiv4.com";
        var clientMqttDevice;

        var optionsforMqttDevice = {
            // port: 8883,
            port: 1883,
            host: mMQTTURL,
            // clientId: 'mqttjs_'+topic,
            clientId: 'mqttjsBTstar_chatBot_Thinga'+topic,
            keepalive: 60,
            reconnectPeriod: 1000,
            protocolId: 'MQIsdp',
            protocolVersion: 3,
            clean: true,
            encoding: 'utf8',
            rejectUnauthorized: false,
            // username: "thingoshub",
            // password: "thingoshub"
            username: "thingaoshub",
            password: "thingaoshub"
        };

        DebugzwaveAddHdlr("#############################");
        DebugzwaveAddHdlr("mqttFunctionForZwaveRemDevStatusCheck");
        DebugzwaveAddHdlr("clientMqttDevice mMQTTURL = " + mMQTTURL);
        //clientMqttDevice = mqttChatBot.connect('mqtts://' + mMQTTURL, optionsforMqttDevice);
        clientMqttDevice = mqttChatBot.connect('mqtt://' + mMQTTURL, optionsforMqttDevice);
        var toCloseMqttConn = false;
        var timeouttoCloseMqttConn;
        clientMqttDevice.on('connect', function(err) {
            DebugzwaveAddHdlr("clientMqttDevice PDDM connect toCloseMqttConn = "+toCloseMqttConn);
            if(toCloseMqttConn == true)
            {
                //To Do
            }
            else
            {
                toCloseMqttConn = true;
                DebugzwaveAddHdlr("clientMqttDevice PDDM connect topic check = "+topic);
                clientMqttDevice.subscribe(topic + '/get/state');

                timeouttoCloseMqttConn = setTimeout(function() {
                    DebugzwaveAddHdlr("clientMqttDeviceBTStar timeouttoCloseMqttConn connected toCloseMqttConn = " + toCloseMqttConn);
                    if(toCloseMqttConn == true) {
                        toCloseMqttConn = false;
                        clientMqttDevice.unsubscribe(topic + '/get/state');
                        clientMqttDevice.end();
                        DebugzwaveAddHdlr("clientMqttDeviceBTStar.end()");
                        var objFail = { status: 'false' };
                        rejectMqtt(objFail);
                    } 
                }, 60000);
            }
        });

        clientMqttDevice.on('message', function (topic, message) {
            DebugzwaveAddHdlr("clientMqttDevice message topic = " + topic);
            DebugzwaveAddHdlr("clientMqttDevice message message = " + message);

            if(topic.indexOf("/get/state") > -1)
            {
                toCloseMqttConn = false;
                clientMqttDevice.unsubscribe(topic);
                var objSuc = { status: 'completed' };
                var objFail = { status: 'false' };
                var messageJsonParsed = JSON.parse(message);
                DebugzwaveAddHdlr("messageJsonParsed = ");
                DebugzwaveAddHdlr(messageJsonParsed);
                DebugzwaveAddHdlr("messageJsonParsed.msg = " + messageJsonParsed.msg);
                DebugzwaveAddHdlr("messageJsonParsed.nodeid = " + messageJsonParsed.nodeid);

                messageJsonParsed.zwaveTemplateList.ZwaveTemplates = messageJsonParsed.zwaveTemplateList.ZwaveTemplates
                    .filter((item) => item.nodeid !== messageJsonParsed.nodeid);
                // messageJsonParsed.zwaveTemplateList.ZwaveTemplates.push(messageJsonParsed.newDeviceObj);
                // DebugzwaveAddHdlr("messageJsonParsed after update = ");
                // DebugzwaveAddHdlr(messageJsonParsed);
                // DebugzwaveAddHdlr("messageJsonParsed.zwaveTemplateList after update = ");
                // DebugzwaveAddHdlr(messageJsonParsed.zwaveTemplateList);
                if(topic.indexOf("ZwaveCheckRemoveStatusChatbot") > -1){
                    //if(message.indexOf("Pairing-Completed")> -1){
                    if(messageJsonParsed.msg.indexOf("Removing-Completed")> -1){
                        clientMqttDevice.publish("ZwavechatBotsendUpdZwaveTemplate/" + HubId,JSON.stringify(messageJsonParsed.zwaveTemplateList));
                        resolveMqtt(objSuc);
                    } else {
                        rejectMqtt(objFail);
                    }
                }

                clearInterval(timeouttoCloseMqttConn);
                clientMqttDevice.end();
            }
        });

        ///////////////

    });
}

exports.zWAveRemoveCheckRemoveStatus = function (HubId, obj, objHeaders) {
    return new Promise(function (resolve, reject) {
        mqttFunctionForZwaveRemDevStatusCheck("ZwaveCheckRemoveStatusChatbot/" + HubId, HubId)
            .then(function (resultMqttExt) {
                DebugzwaveAddHdlr("zWAveRemoveCheckRemoveStatus resultMqttExt :-");
                DebugzwaveAddHdlr(resultMqttExt);
                resolve(resultMqttExt);
            })
            .catch(function(errMqttExt){
                DebugzwaveAddHdlr("zWAveRemoveCheckRemoveStatus errMqttExt :-");
                DebugzwaveAddHdlr(errMqttExt);
                reject(errMqttExt);
            });
    });
};

exports.zWAveRemovedeviceStart = function (HubId, obj, objHeaders) {
    return new Promise(function (resolve, reject) {
        mqttFunctionForZwaveAddRemoveSlot("addremoveZwaveDeviceChatbot/" + HubId, "removeZwaveDevice", HubId)
            .then(function (resultMqttExt) {
                DebugzwaveAddHdlr("zWAveRemovedeviceStart resultMqttExt :-");
                DebugzwaveAddHdlr(resultMqttExt);
                resolve(resultMqttExt);
            })
            .catch(function(errMqttExt){
                DebugzwaveAddHdlr("zWAveRemovedeviceStart errMqttExt :-");
                DebugzwaveAddHdlr(errMqttExt);
                reject(errMqttExt);
            });
    });
};

exports.zWAveResetHubStart = function (HubId, obj, objHeaders) {
    return new Promise(function (resolve, reject) {
        mqttFunctionForZwaveAddRemoveSlot("addremoveZwaveDeviceChatbot/" + HubId, "resetZwaveHub", HubId)
            .then(function (resultMqttExt) {
                DebugzwaveAddHdlr("zWAveResetHubStart resultMqttExt :-");
                DebugzwaveAddHdlr(resultMqttExt);
                resolve(resultMqttExt);
            })
            .catch(function(errMqttExt){
                DebugzwaveAddHdlr("zWAveResetHubStart errMqttExt :-");
                DebugzwaveAddHdlr(errMqttExt);
                reject(errMqttExt);
            });
    });
};


function mqttFunctionForZwaveRTesetHubStatusCheck(topic,HubId) {
    return new Promise(function (resolveMqtt, rejectMqtt) {
        ///////////////

        var JWTTokenHubId;
        var mqttChatBot = require('mqtt');
        // var mMQTTURL = "mqtt-dev.tantiv4.com";
        var mMQTTURL = "ritehub-mqtt.tantiv4.com";
        var clientMqttDevice;

        var optionsforMqttDevice = {
            // port: 8883,
            port: 1883,
            host: mMQTTURL,
            // clientId: 'mqttjs_'+topic,
            clientId: 'mqttjsBTstar_chatBot_Thinga'+topic,
            keepalive: 60,
            reconnectPeriod: 1000,
            protocolId: 'MQIsdp',
            protocolVersion: 3,
            clean: true,
            encoding: 'utf8',
            rejectUnauthorized: false,
            // username: "thingoshub",
            // password: "thingoshub"
            username: "thingaoshub",
            password: "thingaoshub"
        };

        DebugzwaveAddHdlr("#############################");
        DebugzwaveAddHdlr("mqttFunctionForZwaveRTesetHubStatusCheck");
        DebugzwaveAddHdlr("clientMqttDevice mMQTTURL = " + mMQTTURL);
        //clientMqttDevice = mqttChatBot.connect('mqtts://' + mMQTTURL, optionsforMqttDevice);
        clientMqttDevice = mqttChatBot.connect('mqtt://' + mMQTTURL, optionsforMqttDevice);
        var toCloseMqttConn = false;
        var timeouttoCloseMqttConn;
        clientMqttDevice.on('connect', function(err) {
            DebugzwaveAddHdlr("clientMqttDevice PDDM connect toCloseMqttConn = "+toCloseMqttConn);
            if(toCloseMqttConn == true)
            {
                //To Do
            }
            else
            {
                toCloseMqttConn = true;
                DebugzwaveAddHdlr("clientMqttDevice PDDM connect topic check = "+topic);
                clientMqttDevice.publish("get/thingahubUserToken/"+HubId,"");
                clientMqttDevice.subscribe("thingahubUserToken/"+HubId);

                clientMqttDevice.subscribe(topic + '/get/state');

                timeouttoCloseMqttConn = setTimeout(function() {
                    DebugzwaveAddHdlr("clientMqttDeviceBTStar timeouttoCloseMqttConn connected toCloseMqttConn = " + toCloseMqttConn);
                    if(toCloseMqttConn == true) {
                        toCloseMqttConn = false;
                        clientMqttDevice.unsubscribe(topic + '/get/state');
                        clientMqttDevice.unsubscribe("thingahubUserToken/"+HubId);
                        clientMqttDevice.end();
                        DebugzwaveAddHdlr("clientMqttDeviceBTStar.end()");
                        var objFail = { status: 'false' };
                        rejectMqtt(objFail);
                    } 
                }, 60000);
            }
        });

        clientMqttDevice.on('message', function (topic, message) {
            DebugzwaveAddHdlr("clientMqttDevice message topic = " + topic);
            DebugzwaveAddHdlr("clientMqttDevice message message = " + message);

            if(topic.indexOf("thingahubUserToken/") > -1) 
            {
                var messageJsonParsed = JSON.parse(message);
                JWTTokenHubId = messageJsonParsed.token;
                DebugzwaveAddHdlr("thingahubUserToken JWTTokenHubId :- " + JWTTokenHubId);
            }

            if(topic.indexOf("/get/state") > -1)
            {
                toCloseMqttConn = false;
                clientMqttDevice.unsubscribe(topic);
                var objSuc = { status: 'completed' };
                var objFail = { status: 'false' };
                if(topic.indexOf("ZwaveCheckResetStatusChatbot") > -1){
                    if(message.indexOf("Reset-Completed")> -1){
                        resolveMqtt(objSuc);

                        var options = {
                            'method': 'DELETE',
                            'url': 'https://thingaos-server.tantiv4.com/device/'+HubId+'/soft-reset',
                            'headers': {
                              'jwt-token': JWTTokenHubId,
                            }
                          };
                          request(options, function (error, response) { 
                            if (error) throw new Error(error);
                            DebugzwaveAddHdlr("response.body = " + response.body);
                          });

                    } else {
                        rejectMqtt(objFail);
                    }
                }

                clearInterval(timeouttoCloseMqttConn);
                clientMqttDevice.end();
            }
        });

        ///////////////

    });
}

exports.zWAveResetHubCheckStatus = function (HubId, obj, objHeaders) {
    return new Promise(function (resolve, reject) {
        mqttFunctionForZwaveRTesetHubStatusCheck("ZwaveCheckResetStatusChatbot/" + HubId, HubId)
            .then(function (resultMqttExt) {
                DebugzwaveAddHdlr("zWAveResetHubCheckStatus resultMqttExt :-");
                DebugzwaveAddHdlr(resultMqttExt);
                resolve(resultMqttExt);
            })
            .catch(function(errMqttExt){
                DebugzwaveAddHdlr("zWAveResetHubCheckStatus errMqttExt :-");
                DebugzwaveAddHdlr(errMqttExt);
                reject(errMqttExt);
            });
    });
};