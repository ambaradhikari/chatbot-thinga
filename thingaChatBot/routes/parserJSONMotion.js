var variableFile = require("./variable");
var parserJsonHandler = require("./parserJSON");
// var NLPFile = require('./NLP');

var isDebugParserJSONMot = true;
var tag_msg_parserJSONMot = "parserJSONMot :- ";

function DebugMsgParserJSONMot(msg) {
  if (isDebugParserJSONMot) {
    console.log(tag_msg_parserJSONMot, msg);
  }
}

exports.parseWithMotionRulesThen = function(devList, resParse) {
    return new Promise(function(resolve, reject) {  

        var finalString = "";
        DebugMsgParserJSONMot("parseWithMotionRulesThen resParse:- ");
        DebugMsgParserJSONMot(resParse);

        var routineValArr = [];
        var roomValArr = [];
        var emailValArr = [];

        var routineCtrForRoom = 0;
        var roomCtr = 0;
        for (var j = 0; j < resParse.length; j++) {
            var obj = {
                value: "",
                isUsed: false,
                indexVal: 0,
                onlyForRoomRoutine:""
            };
            obj.value = resParse[j].value;
            if (resParse[j].name.indexOf("routine") > -1) {

                if(resParse[j].value.indexOf("turn") > -1){
                    obj.indexVal = ++routineCtrForRoom;
                } else {
                    obj.indexVal = 0;
                }

                routineValArr.push(obj);
            } else if (resParse[j].name.indexOf("room") > -1) {
                obj.indexVal = ++roomCtr;
                roomValArr.push(obj);
            } else if (resParse[j].name.indexOf("email") > -1) {
                emailValArr.push(obj);
            }
        }

        for(var indRout = 0; indRout < routineValArr.length; indRout++){
            for(var indRoom = 0; indRoom < roomValArr.length; indRoom++){
                if(roomValArr[indRoom].indexVal == routineValArr[indRout].indexVal) {
                    roomValArr[indRoom].onlyForRoomRoutine = routineValArr[indRout].value;
                }
            }
        }

        // DebugMsgParserJSONMot("routineValArr:-");
        // DebugMsgParserJSONMot(routineValArr);
        // DebugMsgParserJSONMot("roomValArr:-");
        // DebugMsgParserJSONMot(roomValArr);
        // DebugMsgParserJSONMot("emailValArr:-");
        // DebugMsgParserJSONMot(emailValArr);

        if(roomValArr.length > 0) {
            parserJsonHandler
              .devicesNameUsingRoomNameOrDevNameGroup(devList, roomValArr)
              .then(function(resultDevNameListGrp) {
                // DebugMsgParserJSONMot("resultDevNameListGrp:-");
                // DebugMsgParserJSONMot(resultDevNameListGrp);

                for(var i = 0 ; i < resultDevNameListGrp.length; i++){
                    // DebugMsgParserJSONMot(resultDevNameListGrp[i]);
                    for(var j = 0; j < resultDevNameListGrp[i].value.length; j++){
                        // DebugMsgParserJSONMot(resultDevNameListGrp[i].value[j]);
                        for (var k = 0; k < devList.length; k++) {
                            var deviceVal = devList[k];
                            // DebugMsgParserJSONMot(deviceVal.name);
                            var devicenameLowerCase = resultDevNameListGrp[i].value[j].toLowerCase();
                            if (devicenameLowerCase.indexOf(deviceVal.name.toLowerCase()) > -1) {
                                // DebugMsgParserJSONMot(deviceVal.id);
                                if (j == resultDevNameListGrp[i].value.length - 1 && i==resultDevNameListGrp.length -1)
                                    finalString += resultDevNameListGrp[i].onlyForRoomRoutine + " " + deviceVal.id;
                                else 
                                    finalString += resultDevNameListGrp[i].onlyForRoomRoutine + " " + deviceVal.id + " and ";
                            }
                        }
                    }
                }

                for (var rInd = 0; rInd < routineValArr.length; rInd++){
                    var routineVal = routineValArr[rInd].value;
                    if (routineVal.indexOf("email") > -1) {
                        finalString +=
                        ' and send mail to:"' +
                        emailValArr[0].value +
                        '" subject:"Thinga Detected Motion" text:"Thinga Detected Motion."';
                    } 
                }

                if (finalString != "") {
                    DebugMsgParserJSONMot("(room val> 0)finalString = " + finalString);
                    resolve([finalString,resultDevNameListGrp]);
                } else {
                    DebugMsgParserJSONMot("(room val> 0) finalString is empty, No rule to Make");
                    reject("finalString is empty, No rule to Make");
                }
              })
              .catch(function(errDevNameListGrp) {
                DebugMsgParserJSONMot("errDevNameListGrp");
                DebugMsgParserJSONMot(errDevNameListGrp);
                reject("Device or rooms Not found");
              });
        } else {
            for (var rInd = 0; rInd < routineValArr.length; rInd++){
                var routineVal = routineValArr[rInd].value;
                if (routineVal.indexOf("email") > -1) {
                    finalString +=
                    'send mail to:"' +
                    emailValArr[0].value +
                    '" subject:"Thinga Detected Motion" text:"Thinga Detected Motion."';
                } 
            }
            
            if (finalString != "") {
                DebugMsgParserJSONMot("finalString = " + finalString);
                resolve([finalString,null]);
            } else {
                reject("finalString is empty, No rule to Make");
            }
        }
    });
};

exports.parseWithMotionRulesWhen = function(devList, resParse) {
  return new Promise(function(resolve, reject) {
    DebugMsgParserJSONMot("parseWithMotionRulesWhen resParse:- ");
    DebugMsgParserJSONMot(resParse);

    var finalString = "";

    var defectVal = "";
    var timeVal = "";
    for (var j = 0; j < resParse.length; j++) {
      if (resParse[j].name == "defect") {
        defectVal = resParse[j].value;
        // break;
      }
      if (resParse[j].name == "date-time") {
        timeVal = resParse[j].value;
        // break;
      }
    }

    if (defectVal.indexOf("motion") > -1) {
      defectVal = "Motion Detected";
    } else if (defectVal.indexOf("close") > -1) {
      defectVal = "Door Close";
    } else if (defectVal.indexOf("open") > -1) {
      defectVal = "Door Open";
    } else {
      defectVal = "";
    }
    // DebugMsgParserJSONMot('defectVal = ' + defectVal);

    var deviceVal;
    var deviceSendEvt = [];
    for (var i = 0; i < devList.length; i++) {
      deviceVal = devList[i];

      if (
        deviceVal.config.xPresentLabel &&
        deviceVal.config.xAbsentLabel &&
        (deviceVal.config.xPresentLabel.indexOf(defectVal) > -1 ||
          deviceVal.config.xAbsentLabel.indexOf(defectVal) > -1)
      ) {
        if (
          defectVal.indexOf("Motion Detected") > -1 ||
          defectVal.indexOf("Door Open") > -1
        ) {
          //finalString += "when " + deviceVal.id + " is present then ";
          finalString += "when " + deviceVal.id + " is present";
        } else if (defectVal.indexOf("Door Close") > -1) {
          //finalString += "when " + deviceVal.id + " is absent then ";
          finalString += "when " + deviceVal.id + " is absent";
        }
        break;
      }
    }

    var devFound = false;
    for (i = 0; i < devList.length; i++) {
      deviceVal = devList[i];
      DebugMsgParserJSONMot("deviceVal.name = " + deviceVal.name);
      for(var k = 0 ; k < deviceVal.attributes.length ; k++) {
        var eachAttrib = deviceVal.attributes[k];
        if(defectVal.indexOf("Motion Detected") > -1 && eachAttrib.name == "Home Security") {
          
          deviceSendEvt.push("When Motion sensor  ");
          deviceSendEvt.push(deviceVal.name);
          deviceSendEvt.push(" detects ");
          deviceSendEvt.push("Motion");

          deviceSendEvt.push(deviceVal.id);
          deviceSendEvt.push("btstar|PresencePredicateProvider");
          deviceSendEvt.push("motion-detected");

          finalString += "when " + deviceVal.id + " is present";

          devFound = true;
          break;
        } else if(eachAttrib.name == "Access Control" && defectVal.indexOf("Door") > -1){
          
          deviceSendEvt.push("When Door/Window Sensor  ");
          deviceSendEvt.push(deviceVal.name);
          deviceSendEvt.push(" is ");
          if(defectVal.indexOf("Open") > -1) {
            deviceSendEvt.push("Opened");
          } else {
            deviceSendEvt.push("Closed");
          }

          deviceSendEvt.push(deviceVal.id);
          deviceSendEvt.push("btstar|DoorSensorPredicateProvider");
          if(defectVal.indexOf("Open") > -1) {
            deviceSendEvt.push("door-open-entry-sensor");

            finalString += "when " + deviceVal.id + " is present";
          } else {
            deviceSendEvt.push("door-closed-entry-sensor");

            finalString += "when " + deviceVal.id + " is absent";
          }
          devFound = true;
          break;
        }
      }
      if(devFound) {
        break;
      }
    }

    if (timeVal != "") {
        if(finalString != ""){
            finalString += " and its after "+timeVal;
        } else {
            finalString += "when its after "+timeVal;
        }
    }

    if (finalString != "") {
        finalString += " then ";
        DebugMsgParserJSONMot("finalString = " + finalString);
        resolve([finalString,deviceSendEvt]);
    } else {
        DebugMsgParserJSONMot("finalString not found");
        reject("error parseWithMotionRulesWhen");
    }
  });
};
