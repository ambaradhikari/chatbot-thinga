var MongoClient = require('mongodb').MongoClient;

var mongoose = require("mongoose");
const shortid = require("shortid");

var DeviceSchema = new mongoose.Schema({
    _id: {
        type: String,
        default: shortid.generate
    },
    user_id: {
        type: String,
        ref: "user"
    },
    device: {
        name: String,
        id: String,
        macId: String,
        status: {
            type: String,
            enum: ["offline", "online"],
            default: "offline"
        },
        version: String,
        alternative: String,
        alexaRegion: String
    },
    config: {
        display_name: String,
        isActive: {
            type: Boolean,
            default: true
        },
        defaultDevice: {
            type: Boolean,
            default: true
        },
        isRealDevice: {
            type: Boolean,
            default: true
        }
    },
    deviceList: {},
    pageList: {},
    title: String,
    comments: [],
    alexaSupportedData: {},
    devicesArray: []
}, {
    timestamps: true,
    collection: "device"
});

// var url = "mongodb://localhost:27017/";
// var url = "mongodb://thingaDbUser:9caa5b9774ac0cc9899d7b78f283ae97c150da47bfb87077f9f610528fc68c1e@localhost:27017/";
var url = "mongodb://thingaDbUser:9caa5b9774ac0cc9899d7b78f283ae97c150da47bfb87077f9f610528fc68c1e@localhost:27017/";
var dbName = "thingadb";
var dbDevCollection = "device";

var isDebugMongoDB = true;
var tag_msg_mongoDB = "mongoDB :- ";

function DebugMsgMongoDB(msg) {
    if (isDebugMongoDB) {
        console.log(tag_msg_mongoDB + msg);
    }
}

exports.getDeviceListFromDB = function (hubID) {
    DebugMsgMongoDB('getDeviceListFromDB = ');
    return new Promise(function (resolve, reject) {

        const DeviceModel = mongoose.model("device", DeviceSchema);

        DeviceModel.findOne({
            "device.id": hubID
        }, function(err,result) {
            if (err || result === null) {
                reject("error in retrieving Device list from DB of " + hubID + " hub id");
                throw err;
            } else {
                DebugMsgMongoDB("getDeviceListFromDB length = " + result.deviceList.length);
                resolve(result.deviceList);
            }

        });
    });
};

// exports.getDeviceListFromDB = function (hubID) {
//     DebugMsgMongoDB('getDeviceListFromDB = ');
//     return new Promise(function (resolve, reject) {

//         MongoClient.connect(url, {
//                     useUnifiedTopology: true
//                 }, function (err, db) {
//             if (err) throw err;
//             var dbo = db.db(dbName);
//             // db.device.find({"device.id":"0474162a31a2892"}).pretty()
//             dbo.collection(dbDevCollection).findOne({
//                 "device.id": hubID
//             }, function (err, result) {
//                 if (err) {
//                     reject("error in retrieving Device list from DB of " + hubID + " hub id");
//                     throw err;
//                 } else {
//                     resolve(result.deviceList);
//                 }
                
//                 db.close();
//             });
//         });

//     });
// };
