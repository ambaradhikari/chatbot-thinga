///////////////

var mqttChatBot = require('mqtt');
var mMQTTURL = "mqtt-dev.tantiv4.com";
var clientMqttDevice;
var isConnected = false;

var isDebugMQTT = true;
var tag_msg_MQTT = "MQTT :- ";

function DebugMsgMQTT(msg) {
    if (isDebugMQTT) {
        console.log(tag_msg_MQTT + msg);
    }
}

var optionsforMqttDevice = {
    port: 8883,
    host: mMQTTURL,
    clientId: 'mqttjs_chatBot_Thinga',
    keepalive: 60,
    reconnectPeriod: 1000,
    protocolId: 'MQIsdp',
    protocolVersion: 3,
    clean: true,
    encoding: 'utf8',
    rejectUnauthorized: false,
    username: "thingoshub",
    password: "thingoshub"
};

DebugMsgMQTT("clientMqttDevice mMQTTURL = " + mMQTTURL);
clientMqttDevice = mqttChatBot.connect('mqtts://' + mMQTTURL, optionsforMqttDevice);

clientMqttDevice.on('connect', function (err) {
    DebugMsgMQTT("clientMqttDevice connected ");
    isConnected = true;
    //clientMqttDevice.subscribe('thingoshubUpdDeviceVal/0474162a31a2892');
});

clientMqttDevice.on('message', function (topic, message) {
    DebugMsgMQTT("clientMqttDevice message topic = " + topic);
    DebugMsgMQTT("clientMqttDevice message message = " + message);
});



var mMQTTURLBTStar = "ritehub-mqtt.tantiv4.com";
var clientMqttDeviceBTStar;
var isConnectedBTStar = false;

var optionsforMqttDeviceBTStar = {
    port: 1883,
    host: mMQTTURLBTStar,
    clientId: 'mqttjsBTstar_chatBot_Thinga',
    keepalive: 60,
    reconnectPeriod: 1000,
    protocolId: 'MQIsdp',
    protocolVersion: 3,
    clean: true,
    encoding: 'utf8',
    rejectUnauthorized: false,
    username: "thingaoshub",
    password: "thingaoshub"
};

DebugMsgMQTT("clientMqttDeviceBTStar mMQTTURLBTStar = " + mMQTTURLBTStar);
clientMqttDeviceBTStar = mqttChatBot.connect('mqtt://' + mMQTTURLBTStar, optionsforMqttDeviceBTStar);

clientMqttDeviceBTStar.on('connect', function (err) {
    DebugMsgMQTT("clientMqttDeviceBTStar connected ");
    isConnectedBTStar = true;
    //clientMqttDeviceBTStar.subscribe('thingoshubUpdDeviceVal/0474162a31a2892');
});

clientMqttDeviceBTStar.on('message', function (topic, message) {
    DebugMsgMQTT("clientMqttDeviceBTStar message topic = " + topic);
    DebugMsgMQTT("clientMqttDeviceBTStar message message = " + message);
});

///////////////

/////////////////////////////////////////////////////////////////////////////////////

var express = require('express');
var bodyParser = require('body-parser');
var app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));
app.get('/getTest', function (req, res) {
    console.log("app get getTest");
    res.send('app get getTest');
});
app.post('/postTest', function (req, res) {
    console.log("app post postTest");
    console.log("//////////////////////////////////////////////");
    console.log(req.body);
    console.log("//////////////////////////////////////////////");
    res.send('app post postTest');
});
app.post('/publishMqttMsg', function (req, resCheck) {
    console.log("app post publishMqttMsg");
    console.log("//////////////////////////////////////////////");
    console.log(req.body);
    exports.publishMqttMsg(req.body.topicVal, req.body.msgVal)
        .then(function(result) {
            resCheck.send("success");
        })
        .catch(function(err) {
            resCheck.send("error"); //to do properly
        });
    console.log("//////////////////////////////////////////////");
    // resCheck.send('app post postTest');
});
var server = app.listen(3089, function () {
    var host = server.address().address;
    var port = server.address().port;
    console.log("Example app listening at http://%s:%s", host, port);
});
/////////////////////////////////////////////////////////////////////////////////////

exports.publishMqttMsg = function (topic, msg) {
    DebugMsgMQTT("publishMqttMsg topic = " + topic);
    DebugMsgMQTT("publishMqttMsg msg = " + msg);
    DebugMsgMQTT("publishMqttMsg isConnected = " + isConnected);
    return new Promise(function (resolve, reject) {
        if (isConnected || isConnectedBTStar) {
            for (var i = 0; i < msg.length;i++){
                clientMqttDevice.publish(topic, JSON.stringify(msg[i]));
                clientMqttDeviceBTStar.publish(topic, JSON.stringify(msg[i]));
            }
            // clientMqttDevice.publish(topic, JSON.stringify(msg));
            // clientMqttDeviceBTStar.publish(topic, JSON.stringify(msg));
            resolve("success");
        } else {
            reject("error:- Not connected");
        }
    });
};

exports.subscribeMqtt = function (topic) {
    if (isConnected) {
        clientMqttDevice.unsubscribe(topic);
        clientMqttDevice.subscribe(topic);
    }

    if (isConnectedBTStar) {
        clientMqttDeviceBTStar.unsubscribe(topic);
        clientMqttDeviceBTStar.subscribe(topic);
    }
};