var request = require('request');
var url = 'http://localhost';
var headers = {
    'Content-Type': 'application/json'
};
var options = {
    headers: headers,
};

exports.publishMqttMsgExtender = function (topic,msg) {
    return new Promise(function (resolve, reject) {
        var options_post = {
            url: url + ':3089/publishMqttMsg',
            form: {
                topicVal: topic,
                msgVal: msg
            }
        };

        request.post(options_post, function (error, response, body) {
            if (!error && response.statusCode == 200) {
            //  console.log("response.statusCode == 200");
            //  console.log('request post = ', body);
                resolve("success");
            } else {
            //  console.log("response.statusCode != 200  error");
            //  console.log(error);
                reject("error:- Not connected");
            }
        });
    });
};