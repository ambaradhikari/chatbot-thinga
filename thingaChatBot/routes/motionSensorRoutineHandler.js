var request = require('request'); 
var mongoDBHandler = require("./mongoDBOperations");
var parserJsonMotHandler = require("./parserJSONMotion");
var variableFile = require("./variable");
var checkExpressExtender = require('./expressExtendOperation');

var isDebugMotSenHdlr = true;
var tag_msg_isDebugMotSenHdlr = "MotSenHandler :- ";

function DebugMotSenHdlr(msg) {
    if (isDebugMotSenHdlr) {
        console.log(tag_msg_isDebugMotSenHdlr, msg);
    }
}

function createBodyForRule(CtrlID, parseJsonFromChatbot, roomNameListWithCtrlpred, roomNameListWithCtrl) {
    return new Promise(function (resolveBodyRule, rejectBodyRule) {

        DebugMotSenHdlr("@$@$@$@$@#$@$@$@$@$@$@$@@$");
        DebugMotSenHdlr("createBodyForRule");
        DebugMotSenHdlr("CtrlID:-");
        DebugMotSenHdlr(CtrlID);
        DebugMotSenHdlr("parseJsonFromChatbot:-");
        DebugMotSenHdlr(parseJsonFromChatbot);
        DebugMotSenHdlr("roomNameListWithCtrl");
        DebugMotSenHdlr(roomNameListWithCtrl);
        DebugMotSenHdlr("roomNameListWithCtrlpred");
        DebugMotSenHdlr(roomNameListWithCtrlpred);

        var i , j;
        var classProvider;
        var triggerDevVal = {};
        var inputVal = [];
        for (i in parseJsonFromChatbot) {
            if(parseJsonFromChatbot[i].name == "date-time") {
                classProvider = "btstar|CronPredicateProvider";
                inputVal.push("Schedule time at ");
                inputVal.push(parseJsonFromChatbot[i].value + " ");
                break;
            }

            if(parseJsonFromChatbot[i].name == "defect") {
                inputVal.push(roomNameListWithCtrlpred[0]);
                inputVal.push(roomNameListWithCtrlpred[1]);
                inputVal.push(roomNameListWithCtrlpred[2]);
                inputVal.push(roomNameListWithCtrlpred[3]);

                triggerDevVal = {
                    triggerDeviceId : "",
                    triggerValue: ""
                };
                triggerDevVal.triggerDeviceId = roomNameListWithCtrlpred[4];
                classProvider = roomNameListWithCtrlpred[5];
                triggerDevVal.triggerValue = roomNameListWithCtrlpred[6];
            }
        }

        var actionTextVal = [];
        for (i in roomNameListWithCtrl) {
            for (j in roomNameListWithCtrl[i].id) {
                actionTextVal.push(roomNameListWithCtrl[i].onlyForRoomRoutine + " " +  roomNameListWithCtrl[i].id[j]);
            } 
        }
        

        // {
        //     "actionText": [
        //       "turn on 200"
        //     ],
        //     "ruleName": "time based schedule",
        //     "inputValue": [
        //       "Schedule time at ",
        //       "15:03 "
        //     ],
        //     "predicateProviderClass": "btstar|CronPredicateProvider",
        //     "locationSelected": "always",
        //     "triggerDevice": {},
        //     "controller_id": "oRusMDpEI"
        //   }
        var m = JSON.parse("{}");
        m.actionText = [];
        m.actionText = actionTextVal;
        m.ruleName = "ChatBotRule_"+ Math.random().toString(16).substr(2, 9);
        m.inputValue = [];
        m.inputValue = inputVal;
        m.predicateProviderClass = classProvider;
        m.locationSelected = "always";
        m.triggerDevice = {};
        m.triggerDevice = triggerDevVal;
        m.controller_id = CtrlID;

        var finalBody = m;
        resolveBodyRule(finalBody);
    });
}

function mqttFunctionForAddingRule(HubId , parseJsonFromChatbot, roomNameListWithCtrlpred, roomNameListWithCtrl) {
    return new Promise(function (resolveMqtt, rejectMqtt) {
        ///////////////

        var JWTTokenHubId;
        var mqttChatBot = require('mqtt');
        // var mMQTTURL = "mqtt-dev.tantiv4.com";
        var mMQTTURL = "ritehub-mqtt.tantiv4.com";
        var clientMqttDevice;

        var optionsforMqttDevice = {
            // port: 8883,
            port: 1883,
            host: mMQTTURL,
            // clientId: 'mqttjs_'+topic,
            clientId: 'mqttjsBTstar_chatBot_Thinga_AddRule_'+HubId,
            keepalive: 60,
            reconnectPeriod: 1000,
            protocolId: 'MQIsdp',
            protocolVersion: 3,
            clean: true,
            encoding: 'utf8',
            rejectUnauthorized: false,
            // username: "thingoshub",
            // password: "thingoshub"
            username: "thingaoshub",
            password: "thingaoshub"
        };

        DebugMotSenHdlr("#############################");
        DebugMotSenHdlr("mqttFunctionForAddingRule");
        // DebugMotSenHdlr("parseJsonFromChatbot:-");
        // DebugMotSenHdlr(parseJsonFromChatbot);
        // DebugMotSenHdlr("roomNameListWithCtrl:-");
        // DebugMotSenHdlr(roomNameListWithCtrl);
        DebugMotSenHdlr("clientMqttDevice mMQTTURL = " + mMQTTURL);
        //clientMqttDevice = mqttChatBot.connect('mqtts://' + mMQTTURL, optionsforMqttDevice);
        clientMqttDevice = mqttChatBot.connect('mqtt://' + mMQTTURL, optionsforMqttDevice);
        var toCloseMqttConn = false;
        var timeouttoCloseMqttConn;
        clientMqttDevice.on('connect', function(err) {
            DebugMotSenHdlr("clientMqttDevice PDDM connect toCloseMqttConn = "+toCloseMqttConn);
            if(toCloseMqttConn == true)
            {
                //To Do
            }
            else
            {
                toCloseMqttConn = true;
                DebugMotSenHdlr("clientMqttDevice PDDM connect HubId check = "+HubId);
                clientMqttDevice.subscribe("thingahubUserToken/"+HubId);
                clientMqttDevice.publish("get/thingahubUserToken/"+HubId,"");

                timeouttoCloseMqttConn = setTimeout(function() {
                    DebugMotSenHdlr("clientMqttDeviceBTStar timeouttoCloseMqttConn connected toCloseMqttConn = " + toCloseMqttConn);
                    if(toCloseMqttConn == true) {
                        toCloseMqttConn = false;
                        clientMqttDevice.unsubscribe("thingahubUserToken/"+HubId);
                        clientMqttDevice.end();
                        DebugMotSenHdlr("clientMqttDeviceBTStar.end()");
                        var objFail = { status: 'false' };
                        rejectMqtt(objFail);
                    } 
                }, 60000);
            }
        });

        clientMqttDevice.on('message', function (topic, message) {
            DebugMotSenHdlr("clientMqttDevice message topic = " + topic);
            // DebugMotSenHdlr("clientMqttDevice message message = " + message);

            if(topic.indexOf("thingahubUserToken/") > -1) 
            {
                var messageJsonParsed = JSON.parse(message);
                JWTTokenHubId = messageJsonParsed.token;
                DebugMotSenHdlr("thingahubUserToken JWTTokenHubId :- " + JWTTokenHubId);

                var options = {
                    'method': 'GET',
                    'url': 'https://thingaos-server.tantiv4.com/device/'+HubId,
                    'headers': {
                      'jwt-token': JWTTokenHubId,
                    }
                  };
                  request(options, function (error, response) { 
                    if (error) throw new Error(error);
                    var JSONBody = JSON.parse(response.body);
                    if(JSONBody && JSONBody.device && JSONBody.device._id) {
                        DebugMotSenHdlr("JSONBody device._id = " + JSONBody.device._id);

                        createBodyForRule(JSONBody.device._id, parseJsonFromChatbot, roomNameListWithCtrlpred, roomNameListWithCtrl)
                            .then(function(ResBodyRule) {
                                // DebugMotSenHdlr("ResBodyRule = ");
                                // DebugMotSenHdlr(ResBodyRule);
                                DebugMotSenHdlr("(JSON.stringify(ResBodyRule) = ");
                                DebugMotSenHdlr(JSON.stringify(ResBodyRule));

                                var optionsCreateRule = {
                                    'method': 'POST',
                                    'url': 'https://thingaos-server.tantiv4.com/rules/create-rule',
                                    'headers': {
                                      'jwt-token': JWTTokenHubId,
                                      'Content-Type': 'application/json',
                                    },
                                    body: JSON.stringify(ResBodyRule)
                                };
                                request(optionsCreateRule, function (error, response) { 
                                    if (error) throw new Error(error);
                                    DebugMotSenHdlr("create rule success");
                                    DebugMotSenHdlr(response.body);
                                });

                                var objSuc = { status: 'completed' };
                                resolveMqtt(objSuc);
                            })
                            .catch(function(errBodyRule) {
                                var objFail = { status: 'false' };
                                rejectMqtt(objFail);
                            });
                    }
                  });

                clearInterval(timeouttoCloseMqttConn);
                clientMqttDevice.end();
            }

        });

        ///////////////

    });
}

function parseObjHeaderMotion(obj,objHeaders) {
     return new Promise(function (resolve, reject) {
        var actionHeader = objHeaders.action;
        DebugMotSenHdlr("actionHeader :- " + actionHeader);
        var arrayRulesCheck = [];
        if (obj.slots) {
            var slotsArray = Object.keys(obj.slots);
            for (var i = 0; i < slotsArray.length; i++) {
                var RulesCheck = {};
                RulesCheck.name = slotsArray[i];
                if(slotsArray[i] == "date-time") {
                    if(obj.slots[slotsArray[i]].from){
                        var time = obj.slots[slotsArray[i]].from;
                        var strArr = time.split("T");
                        str = strArr[1];
                        RulesCheck.value = str.substring(0, 5);
                    } else {
                        RulesCheck.value = obj.slots[slotsArray[i]].value;
                    }
                } else {
                    RulesCheck.value = obj.slots[slotsArray[i]].value;
                }
                if (RulesCheck.value == undefined || RulesCheck.value == null || RulesCheck.value == "") {
                    //toDo
                } else {
                    arrayRulesCheck.push(RulesCheck);
                }
            }
        }

        if(obj.slots && actionHeader.indexOf("action_schedule") > -1){
            DebugMotSenHdlr("actionHeader value:- " + variableFile.actionStr[actionHeader].action);
            var RulesCheckSch = {};
            RulesCheckSch.name = "routine_action_sch";
            RulesCheckSch.value = variableFile.actionStr[actionHeader].action;
            arrayRulesCheck.push(RulesCheckSch);
        }

        if (arrayRulesCheck.length > 0) {
            resolve(arrayRulesCheck);
        } else {
            reject("error parseObjHeaderMotion");
        }
     });
}

exports.handlingValuesMotionSensor = function (obj, objHeaders, HubId) {
    return new Promise(function (resolveMain, rejectMain) {

        DebugMotSenHdlr("HubId :- " + HubId);

        var result;
        var resultMongoGlb;
        var resultParseMotionGlb;
        var resultParserJsonMotWhenGlb;
        var resultParserJsonMotThenGlb;
        var resultParserJsonMotWhenGlbRoomVar;
        var resultParserJsonMotThenGlbRoomVar;

         mongoDBHandler.getDeviceListFromDB(HubId)
            .then(function(resultMongo) {
                resultMongoGlb = resultMongo;
                DebugMotSenHdlr("promisegetDeviceListFromDB resultDeviceListfromDB received");
                return parseObjHeaderMotion(obj,objHeaders);
            })
            .then(function(resultParseMotion) {
                resultParseMotionGlb = resultParseMotion;
                // DebugMotSenHdlr("resultParseMotion :- ");
                // DebugMotSenHdlr(resultParseMotionGlb);
                return parserJsonMotHandler.parseWithMotionRulesWhen(resultMongoGlb, resultParseMotionGlb);
            })
            .then(function (resultParserJsonMotWhen) {
                DebugMotSenHdlr("resultParserJsonMotWhen :- ");
                // resultParserJsonMotWhenGlb = resultParserJsonMotWhen;
                resultParserJsonMotWhenGlb = resultParserJsonMotWhen[0];
                resultParserJsonMotWhenGlbRoomVar = resultParserJsonMotWhen[1];
                DebugMotSenHdlr(resultParserJsonMotWhenGlb);
                DebugMotSenHdlr(resultParserJsonMotWhenGlbRoomVar);
                return parserJsonMotHandler.parseWithMotionRulesThen(resultMongoGlb, resultParseMotionGlb);
            })
            .then(function (resultParserJsonMotThen) {
                DebugMotSenHdlr("resultParserJsonMotThen :- ");
                resultParserJsonMotThenGlb = resultParserJsonMotThen[0];
                resultParserJsonMotThenGlbRoomVar = resultParserJsonMotThen[1];
                DebugMotSenHdlr("resultParserJsonMotThenGlb:-");
                DebugMotSenHdlr(resultParserJsonMotThenGlb);
                // DebugMotSenHdlr("resultParserJsonMotThenGlbRoomVar:-");
                // DebugMotSenHdlr(resultParserJsonMotThenGlbRoomVar);
                var finalList = [];
                finalList.push(resultParserJsonMotWhenGlb + resultParserJsonMotThenGlb);
                return checkExpressExtender.publishMqttMsgExtender("rulesHandleChatbot/" + HubId, finalList);
            })
            .then(function(resultMqttExt) {
                DebugMotSenHdlr("resultMqttExt :- ");
                DebugMotSenHdlr(resultMqttExt); 

                mqttFunctionForAddingRule(HubId, resultParseMotionGlb, resultParserJsonMotWhenGlbRoomVar, resultParserJsonMotThenGlbRoomVar)
                    .then(function (resultMqttAddRule) {
                        DebugMotSenHdlr("mqttFunctionForAddingRule resultMqttAddRule :-");
                        DebugMotSenHdlr(resultMqttAddRule);
                    })
                    .catch(function(errMqttAddRule){
                        DebugMotSenHdlr("mqttFunctionForAddingRule errMqttAddRule :-");
                        DebugMotSenHdlr(errMqttAddRule);
                    });

                resolveMain("the routine is been set as " + resultParserJsonMotWhenGlb + resultParserJsonMotThenGlb);
            })
            .catch(function(err) {
                DebugMotSenHdlr("error :- ");
                DebugMotSenHdlr(err);
                rejectMain("action_motion_routine not handled yet Reject " + err);
            });
    });
};