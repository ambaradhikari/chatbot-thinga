
var mongoDBHandler = require("./mongoDBOperations");
var parserJsonHandler = require("./parserJSON");
var NLPFile = require('./NLP');
var checkExpressExtender = require('./expressExtendOperation');

var colorSchemainMqtt = require("./colorSchema");

var isDebugLgtSwtHdlr = true;
var tag_msg_isDebugLgtSwtHdlr = "LightSwitchHdlr :- ";

function DebugMsgLgtSwtHdlr(msg) {
    if (isDebugLgtSwtHdlr) {
        console.log(tag_msg_isDebugLgtSwtHdlr , msg);
    }
}

exports.handlingValues = function (obj, objHeaders, HubId) {
    return new Promise(function (resolveMain, rejectMain) {

        var result;
        var resultDeviceListfromDB;
        var resultDeviceNameListFromSet = [];

        DebugMsgLgtSwtHdlr("handlingValues");

        mongoDBHandler.getDeviceListFromDB(HubId)
            .then(function (resultDeviceList) {
                resultDeviceListfromDB = resultDeviceList;
                DebugMsgLgtSwtHdlr("promisegetDeviceListFromDB resultDeviceListfromDB received");
                return parserJsonHandler.parseBodyHeader(obj, objHeaders);
            })
            .then(function (resultParse) {
                result = resultParse;
                DebugMsgLgtSwtHdlr("promiseParseBodyHeader response :-");
                DebugMsgLgtSwtHdlr(result);
                if (result.actionType == "set") {
                    return parserJsonHandler.devicesNameUsingRoomNameOrDevName(resultDeviceListfromDB, result);
                } else {
                    DebugMsgLgtSwtHdlr("promiseParseBodyHeader get values");
                    throw new Error("Get Values");
                }

            })
            // .then(function (resultDeviceNameList) {
            .then(function (resultDeviceList) {
                var finalList = [];
                // resultDeviceNameListFromSet = resultDeviceNameList;
                var resultDeviceNameList = resultDeviceList[0];
                var resultDeviceIdList = resultDeviceList[1];
                resultDeviceNameListFromSet = resultDeviceList[0];
                DebugMsgLgtSwtHdlr("resultDeviceNameListFromSet :- ");
                DebugMsgLgtSwtHdlr(resultDeviceNameListFromSet);
                DebugMsgLgtSwtHdlr("resultDeviceIdList :- ");
                DebugMsgLgtSwtHdlr(resultDeviceIdList);
                // for (var i = 0; i < resultDeviceNameList.length; i++) {
                //     var finalVal = "CloudAPI," + result.actionName + "," + resultDeviceNameList[i] + ",querySomething," + result.actionValue;
                //     DebugMsgLgtSwtHdlr("promiseParseBodyHeader finalVal pushing :- ");
                //     DebugMsgLgtSwtHdlr(finalVal);
                //     finalList.push(finalVal);
                // }

                for (var i = 0; i < resultDeviceIdList.length; i++) {
                    var finalVal = "CloudAPI," + result.actionName + "," + resultDeviceIdList[i] + ",querySomething," + result.actionValue;
                    if(result.actionName == "color" && result.actionValue[0] != "#") {
                        finalVal = "CloudAPI," + result.actionName + "," + resultDeviceIdList[i] + ",querySomething," + colorSchemainMqtt[result.actionValue];
                    }
                    DebugMsgLgtSwtHdlr("promiseParseBodyHeader finalVal pushing :- ");
                    DebugMsgLgtSwtHdlr(finalVal);
                    finalList.push(finalVal);
                }
                // DebugMsgLgtSwtHdlr("promiseParseBodyHeader finalVal = ", finalVal);
                // return mqttInit.publishMqttMsg("thingoshub/" + HubId, finalVal);
                //return mqttInit.publishMqttMsg("thingoshub/" + HubId, finalList);
                return checkExpressExtender.publishMqttMsgExtender("thingoshub/" + HubId, finalList);
            })
            .then(function (resultMqtt) {
                DebugMsgLgtSwtHdlr("promiseMqttPublish response :- ");
                DebugMsgLgtSwtHdlr(resultMqtt);

                /////////////////// DO Only for BT star(later for Thinga)
                var mqttChatBotSep = require('mqtt');
                var mMQTTURLBTStar = "ritehub-mqtt.tantiv4.com";
                var clientMqttDeviceBTStar;

                var optionsforMqttDeviceBTStar = {
                    port: 1883,
                    host: mMQTTURLBTStar,
                    clientId: 'mqttjsBTstar_chatBot_Thinga-'+HubId,
                    keepalive: 60,
                    reconnectPeriod: 1000,
                    protocolId: 'MQIsdp',
                    protocolVersion: 3,
                    clean: true,
                    encoding: 'utf8',
                    rejectUnauthorized: false,
                    username: "thingaoshub",
                    password: "thingaoshub"
                };

                DebugMsgLgtSwtHdlr("clientMqttDeviceBTStar mMQTTURLBTStar = " + mMQTTURLBTStar);
                clientMqttDeviceBTStar = mqttChatBotSep.connect('mqtt://' + mMQTTURLBTStar, optionsforMqttDeviceBTStar);

                var toCloseMqttConn = false;
                var timeouttoCloseMqttConn;
                clientMqttDeviceBTStar.on('connect', function (err) {
                    DebugMsgLgtSwtHdlr("clientMqttDeviceBTStar connected toCloseMqttConn = " + toCloseMqttConn);

                    if(toCloseMqttConn == true) {
                        toCloseMqttConn = false;
                        clientMqttDeviceBTStar.unsubscribe("thingoshubDeviceListValue/"+HubId);
                        clientMqttDeviceBTStar.end();
                    } else {
                        toCloseMqttConn = true;
                        clientMqttDeviceBTStar.subscribe("thingoshubDeviceListValue/"+HubId);

                        timeouttoCloseMqttConn = setTimeout(function() {
                            DebugMsgLgtSwtHdlr("clientMqttDeviceBTStar timeouttoCloseMqttConn connected toCloseMqttConn = " + toCloseMqttConn);
                            if(toCloseMqttConn == true) {
                                toCloseMqttConn = false;
                                clientMqttDeviceBTStar.unsubscribe("thingoshubDeviceListValue/"+HubId);
                                clientMqttDeviceBTStar.end();
                                DebugMsgLgtSwtHdlr("clientMqttDeviceBTStar.end()");
                                rejectMain("Device is offline or switched off");
                            } 
                        }, 30000);
                    }
                });
                
                clientMqttDeviceBTStar.on('message', function (topic, message) {
                    DebugMsgLgtSwtHdlr("clientMqttDeviceBTStar topic = " + topic);
                    DebugMsgLgtSwtHdlr("clientMqttDeviceBTStar message = " + message);
                    
                    if(topic.indexOf("thingoshubDeviceListValue/") > -1) {
                        DebugMsgLgtSwtHdlr("topic matched with thingoshubDeviceListValue");
                        var deviceID = topic.split("/")[1];
                        DebugMsgLgtSwtHdlr("clientMqttDeviceBTStar deviceID = " + deviceID);
                        DebugMsgLgtSwtHdlr("clientMqttDeviceBTStar HubId = " + HubId);
                        if(deviceID == HubId) {
                            DebugMsgLgtSwtHdlr("deviceID == HubId --> Do the rest");

                            setTimeout(function () {
                                mongoDBHandler.getDeviceListFromDB(HubId)
                                    .then(function (resultDeviceList) {
                                        // resultDeviceListfromDB = resultDeviceList;
                                        DebugMsgLgtSwtHdlr("Set Values check resultDeviceNameListFromSet :- ");
                                        DebugMsgLgtSwtHdlr(resultDeviceNameListFromSet);
                                        var resObjSend = {
                                            actionName: result.actionName,
                                            actionType: result.actionType,
                                            deviceNameOriginal: result.deviceName,
                                            deviceName: resultDeviceNameListFromSet,
                                            actionValue: result.actionValue
                                        };
                                        DebugMsgLgtSwtHdlr("Set Values check result :- ");
                                        DebugMsgLgtSwtHdlr(resObjSend);
            
                                        return parserJsonHandler.getValueFromDBlist(HubId, resObjSend, resultDeviceList, "set");
                                    })
                                    .then(function (resultJsonHandler) {
                                        DebugMsgLgtSwtHdlr("resultJsonHandler = ");
                                        DebugMsgLgtSwtHdlr(resultJsonHandler);
                                        return NLPFile.finalStringPreparation(resultJsonHandler);
                                        // res.send("resultJsonHandler = " + JSON.stringify(resultJsonHandler));
                                    })
                                    .then(function (resFinalString) {
                                        DebugMsgLgtSwtHdlr("Value resFinalString = ");
                                        DebugMsgLgtSwtHdlr(resFinalString);
                                        resolveMain(resFinalString);
                                        // res.send("resultJsonHandler = " + resFinalString);
                                    })
                                    .catch(function (errJsonHandler) {
                                        DebugMsgLgtSwtHdlr("errJsonHandler = ");
                                        DebugMsgLgtSwtHdlr(errJsonHandler);
                                        rejectMain(errJsonHandler);
                                        // es.send("errJsonHandler = " + errJsonHandler);
                                    });
                            }, 1000);
                        }
                        DebugMsgLgtSwtHdlr("clientMqttDeviceBTStar.end() sep");
                        clearInterval(timeouttoCloseMqttConn);
                        clientMqttDeviceBTStar.end();
                    }

                });

                ///////////////////

            })
            .catch(function (err) {
                DebugMsgLgtSwtHdlr("index.js err = " + err);
                if (err.toString().indexOf("Get Values") > -1) {

                    parserJsonHandler.devicesNameUsingRoomNameOrDevName(resultDeviceListfromDB, result)
                        //.then(function (resultDeviceNameList) {
                        .then(function (resultDeviceList) {
                            DebugMsgLgtSwtHdlr("Get Values check resultDeviceNameList = ");
                            var resultDeviceNameList = resultDeviceList[0];
                            DebugMsgLgtSwtHdlr(resultDeviceNameList);

                            var resObjSend = {
                                actionName: result.actionName,
                                actionType: result.actionType,
                                deviceNameOriginal: result.deviceName,
                                deviceName: resultDeviceNameList,
                                actionValue: result.actionValue
                            };
                            DebugMsgLgtSwtHdlr("Get Values check result = ");
                            DebugMsgLgtSwtHdlr(resObjSend);

                            return parserJsonHandler.getValueFromDBlist(HubId, resObjSend, resultDeviceListfromDB, "get");
                        })
                        .then(function (resultJsonHandler) {
                            DebugMsgLgtSwtHdlr("resultJsonHandler = ");
                            DebugMsgLgtSwtHdlr(resultJsonHandler);
                            return NLPFile.finalStringPreparation(resultJsonHandler);
                            // res.send("resultJsonHandler = " + JSON.stringify(resultJsonHandler));
                        })
                        .then(function (resFinalString) {
                            DebugMsgLgtSwtHdlr("Value resFinalString = ");
                            DebugMsgLgtSwtHdlr(resFinalString);
                            resolveMain(resFinalString);
                            //res.send("resultJsonHandler = " + resFinalString);
                        })
                        .catch(function (errJsonHandler) {
                            DebugMsgLgtSwtHdlr("errJsonHandler = ");
                            DebugMsgLgtSwtHdlr(errJsonHandler);
                            rejectMain(errJsonHandler);
                            //res.send("errJsonHandler = " + errJsonHandler);
                        });

                } else {
                    rejectMain(err);
                    //res.send("error :- " + err);
                }

            });
    });
};