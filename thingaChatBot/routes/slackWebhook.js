var Slack = require('slack-node');
var webhookUri = 'https://hooks.slack.com/services/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';
var slack = new Slack();
slack.setWebhook(webhookUri);

exports.slackWebhookcall = function (body, headers) {
    return new Promise(function (resolve, reject) {
        slack.webhook({
            text: JSON.stringify({
                body: body,
                headers: headers
            })
        }, function (err, response) {
            // console.log(err, response);
            if (err) {
                reject('slackWebhookcall reject = ' + response);
            } else {
                resolve('slackWebhookcall resolve = ' + response);
            }
        });
    });
};